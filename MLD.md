# MLD
12 classes, 40 attributs, 12 assoc 1:N, 4 assoc N:M, 4 vues
#### Bâtiments
Batiments(#nom : VARCHAR, 
		   superficie : REAL, 
		  #localisation_GPS : POINT, 
		   nb_etage : vnb_etage() ENTIER
		   )
			avec 
				nom unique, not null, 
				superficie not null, 
				localisation_GPS unique, not null,    



##### Méthode
1. vnb_etage()


#### Etages
Etages(#num_etage : ENTIER, 
		#batiment => Batiments, 
		#plan_batiment => Imgs
		)
		avec 
			plan_batiment not null AND unique
            (num_etage, batiment) key


#### Salles

Salles(#nom : VARCHAR, 
		#etage => Etages,
        #batiment => Batiments,
		superficie : REAL, 
		capacite_max : ENTIER, 
		#plans_salle => Imgs, 
		carac_tech => Carac_tech
		) 
		avec 
			superficie not null,
			capacite_max not null,
			plans_salle not null AND unique,
			carac_tech not null,
            (nom, etage, batiment) key

##### Méthode
1. vnb_prises_elec_reste()
2. vnb_prises_reseaux_reste()



##### Contrainte compliquée :
1. * [x] CC1: Projectio(Batiments, nom) = Projection(Etages, batiment)
    Batiments 1-- "1..N" Etages
  
2. * [x] CC2: Projection(Etages, num_etage) = Projection(Salles, etage)
    Etages 1-- "1..N" Salles



#### Caractéristiques techniques


Carac_tech(#id : ENTIER, 
			puissance_elec  : ENTIER, 
			arrivee_elec :  VARCHAR, 
			arrivee_air_comprime  :  VARCHAR,
			arrivee_gaz_specifique  :  VARCHAR,
			nb_prises_elec :ENTIER,
			nb_prises_reseaux  : ENTIER
			)
        avec 
			puissance_elec not null,
			arrivee_elec not null,
			arrivee_air_comprime not null,
			arrivee_gaz_specifique not null,
			nb_prises_elec not null,
			nb_prises_reseaux not null
            

-------------------------

#### Imgs
Imgs(#url: urlWeb,
	  type: ('PE', 'PS', 'LO', 'PH'),
	  #serveur => PC_serveurs
	  salle => Salles,
	  laboratoire => Laboratoires 
)
avec
	(type = 'PH' AND salle NOT NULL AND laboratoire NULL) 
	OR (type = 'PE' | 'PS' AND salle NULL AND laboratoire NULL)
	OR (type = 'LO' AND salle NULL AND laboratoire NOT NULL)


##### Vue
1. vPlan_etage :Restriction(Imgs, type = 'PE')
2. vPlan_salle :Restriction(Imgs, type = 'PS')
3. vLogo :Restriction(Imgs, type = 'LO')
4. vPhoto :Restriction(Imgs, type = 'PH')

-----------------

#### Employes

Employes(#num_badge : VARCHAR,
          nom : VARCHAR,
          prenom : VARCHAR,
          #email : VARCHAR,
          statut_employe => Statut_employe 
          bureau: Salles,
          organisation: Organisations,
          organisation_secondaire: Organisations

)
avec 
    nom not null,                  
    prenom not null,                        
    email not null AND unique,
    statut_employe not null AND unique,
    bureau not null,       
    organisation not null

Statut_employe(#type : VARCHAR)
(CDI, CDD, Stagiaire, etc.),

##### Contrainte compliquée 


2. * [x] CC2: 
          Intersection(Restriction( Projection(Jointure(Employes, Organisations, E.organisation = O.sigle), type) , organisation_secondaire != null) ,Projection(Jointure(Employes, Organisations, E.organisation_secondaire = O.sigle), type)) = Ø


##### Vue

1. * [x] vDirecteur = Projection (Jointure(Employes, Organisations, E.num_badge = O.directeur), E)
2. * [ ] vMembre = Projection(Jointure(Employes, Organisations, E.organisation = O.sigle), E)

-------------------------

#### Machines de fabrication et delaboratoire
Machines(#num_contrat : VARCHAR,
          modele => Besoins,
          description : text,
          entreprise_maintenance: VARCHAR,
          taille_machine: ENTIER,
          salle => Salles
)
avec 
    taille_machine not null,                  
    salle not null,
    modele not null                          

Besoins(#id : ENTIER,
        puissance_elec : ENTIER,
        besoin_triphase : BOOLEAN,
        besoin_reseau : BOOLEAN,
        besoin_gaz: VARCHAR
)
avec 
    puissance_elec not null,                  
    besoin_triphase not null,                        
    besoin_reseau not null,          
    besoin_gaz not null,       



---------------------

#### Postes téléphoniques

Postes_tele(#num_interne: VARCHAR,
            #num_direct : VARCHAR,
             type : VARCHAR(1) (V, T, L),
             modele_tele : VARCHAR,
             marque : VARCHAR,
             bureau => Salles
)
avec 
    modele_tele not null,                  
    num_interne not null AND unique,                        
    num_direct not null,          
    marque not null,       
    bureau not null,
    type = 'V' OR type = 'T' OR type = 'L',
    (num_interne, num_direct) key

--------------------------

#### Moyens informatiques

PC_fixes(#nom : VARCHAR,
          OS : VARCHAR (Linux, Windows, Mac),
          responsable => Employes,
          projet => Projets,
          machine => Machines,
          adresse_MAC : VARCHAR,
          adresse_IP : VARCHAR,
          salle => Salles
)
avec
    salle not null,
    responsable not null,
    adresse_MAC not null AND unique,
    adresse_IP not null AND unique,
    OS not null



PC_portables(#nom : VARCHAR,
            OS : VARCHAR (Linux, Windows, Mac),
            responsable => Employes,
            adresse_MAC : VARCHAR,
            adresse_IP : VARCHAR,
            ordi_fonction => Employes
            salle => Salles
)
avec
    (salle not null AND ordi_fonction null) OR (salle null AND ordi_fonction not null)
    responsable not null,
    adresse_MAC not null AND unique,
    adresse_IP not null AND unique.
    OS not null


PC_serveurs(#nom : VARCHAR,
          responsable => Employes,
          salle => VARCHAR
)
avec
    salle not null,
    responsable not null,
    adresse_MAC not null,
    adresse_IP not null

Adresse_MAC_serveur(#adresse_MAC,
            serveur => PC_serveurs
)
avec
    serveur not null

CC: Projection(Adresse_Mac, serveur) = projection(PC_serveurs, nom)

Adresse_Ip_serveur(#adresse_Ip VARCHAR(15)，
            adresse_Mac => Adresse_MAC_serveur
)
avec
    adresse_Mac not null

##### Contrainte compliquée :

1. * [x] CC1 : Intersection (Projection (PC_fixes), nom), Projection (PC_portables, nom), Projection (PC_serveurs, nom)) = Ø
2. * [x] CC: projection(PC_serveurs, nom) = Projection(Adresse_MAC_serveur, serveur)
     PC_serveurs "1" --- "1..N" Adresse_MAC_serveur : > a

3. * [x] CC: Projection(Adresse_MAC_serveur, adresse_MAC) = projection(Adresse_Ip_serveur, adresse_MAC)
     Adresse_MAC_serveur "1" --- "1..N" Adresse_Ip_serveur : > a

##### Vue:


* [x] vMoyen_Info = Union (Projection (PC_fixes), nom), Projection (PC_portables, nom), Projection (PC_serveurs, nom))

-------------------------

#### Organisations

Organisations(#sigle : VARCHAR,
               nom : VARCHAR,
               directeur => Employes,
               Type: VARCHAR(1) ("L", "D"),
               logo => Imgs,
               thematique_etude => Thematiques_etude
               domaine_expertise => Domaines_expertise
)
avec
    directeur unique,
    adresse_MAC not null,
    adresse_IP not null,
    (type = "L" AND logo NOT NULL AND unique AND thematiques_etude NOT NULL AND domaines_expertise NULL) OR (type = "D AND logo NULL AND thematiques_etude NULL AND domaines_expertise NOT NULL AND unique)

Thematiques_etude(#type : VARCHAR)
Domaines_expertise(#type : VARCHAR)

##### Vue:

* [x] vLaboratoires = Restriction(Organisations, type = "L")
* [x] vDepartements = Restriction(Organisations, type = "D")

---------------------------------

#### Projets


Projets(#sigle : VARCHAR,
        #nom : VARCHAR,
         description : VARCHAR
         start_date : DATE,
         end_date : DATE,
         chef => Employes,
)
avec
    start_date not null,
    end_date null OR end_date > start_date
    nom unique AND not null,


Taches(#id: ENTIER,
       #projet => Projets,
        start_date : DATE,
        end_date : DATE,
)
avec
    start_date not null,
    end_date not null,
    (id, projet) key

Participe(#projet => Projets,
          #employe => Employes,
          #tache => Taches,
          role : VARCHAR,

)
avec
    role not null,
    (employe, tache) key


##### Contrainte compliquée :

1. * [x] Projection(Projets, sigle) = Projection(Taches, projet)
Projets 1-- "1..N" Taches
2. * [x] Restriction(Jointure(Projets, Taches, p.sigle = t.projet), t.start_date < p.start_date ) = Ø
3. * [x] Restriction(Jointure(Projets, Taches, p.sigle = t.projet), t.end_date > p.end_date ) = Ø


#### Many to many
Projet_moyen(#projet => Projets, #moyen_info => vMoyen_info)
Projets "*" --- "*" Moyens_info : > utilise


Machines_moyen(#machine => Machines, #moyen_info => vMoyen_info)
Machines "*" --- "*" Moyens_info : > utilise

Org_machine(#organisation => Organisations, machine => Machines)
Organisations "*" -- "*" Machines : > utilise



## Normalisation

### 1NF

* Parce que nous permettons aux employés de rejoindre deux types d'organisations différents en même temps. Par exemple, pour rejoindre un laboratoire et un département à la fois, afin d'assurer l'atomicité des attributs, nous avons divisé l'attribut `organisation` de `Employes` en `organisation` et `organisation_secondaire`.

### 2NF

* Pour les exigences matérielles requises par la machine, elle est uniquement liée au modèle de la machine, afin de répondre aux besoins de 2NF, nous extrayons les besoins de la machine de la classe `Machines	` et créons une classe de `Besoin`.
* Même raison qu'on extrait la classe `Carac_tech` depuis la classe `Salles`.

### 3NF

* Chaque colonne de chauqe classe est directement liée à la colonne de clé primaire, pas indirectement, donc on est bien en 3NF.

## JSON

1. Pour plusieurs `adresses MAC`, plusieurs adresses IP sur le serveur et des relations hiérarchiques complexes entre elles, nous pouvons utiliser JSON à la place.

   ```sql
   CREATE TABLE PC_serveurs (
       nom varchar(20) PRIMARY KEY,
   		responsable varchar(20) NOT NULL,
       nom_salle varchar(20) NOT NULL,
       etage_salle int NOT NULL,
       batiment_salle varchar(20) NOT NULL,
       adresse_MAC JSON NOT NULL,
       FOREIGN KEY (nom_salle, etage_salle, batiment_salle) REFERENCES Salles (nom, etage, batiment),
       FOREIGN KEY (responsable) REFERENCES Employes (num_badge)
   );
   
   INSERT INTO PC_serveurs (nom, responsable , nom_salle, etage_salle, batiment_salle, adresse_MAC) VALUES (
       '#4fa757',
       '#5a96a6',
       '#5b3eab',
       3,
       'Green',
      '[
           {
               "adresse_MAC":"CA-D0-1A-75-77-DB", 
               "adresse_IP": "97.164.67.22"
           },
           {
               "adresse_MAC":"CA-D0-1A-75-77-DB", 
               "adresse_IP": "213.27.10.62"
           }
      ]
       '
   );
   ```



2. Notre table des employés a deux attributs, organisation et organisation_secondaire, que nous pouvons simplifier en utilisant JSON.

   ```sql
   CREATE TABLE Employes (
       num_badge varchar(20) PRIMARY KEY,
       nom varchar(20) NOT NULL,
       prenom varchar(20) NOT NULL,
       email varchar(50) NOT NULL UNIQUE,
       statut_employe JSON NOT NULL,
       organisation JSON NOT NULL,
       nom_bureau varchar(20) NOT NULL,
       etage_bureau int NOT NULL,
       batiment_bureau varchar(20) NOT NULL,
       FOREIGN KEY (nom_bureau, etage_bureau, batiment_bureau) REFERENCES Salles (nom, etage, batiment),
       FOREIGN KEY (statut_employe) REFERENCES Statut_employe (TYPE)
   );
   
   INSERT INTO Employes (num_badge, nom, prenom, email, statut_employe, organisation, nom_bureau, etage_bureau, batiment_bureau)
       VALUES ('#2280ef', 'Saltrese', 'Shannan', 'ssaltrese0@pro.jp', 
       '{
           "type": "CDI"
       }', 
       '[
           {
               "sigle":"ML"
           },
           {
               "sigle":"PMD"
           }
       ]', 
       '#13bb79', 1, 'Green'
       );
   ```

   

3. Afin d'exprimer facilement la relation un-à-un, nous avons directement remplacé les deux tables `Carac_tech` et `Besoin` par JSON

   ```sql
   CREATE TABLE Salles (
       nom varchar(20) NOT NULL UNIQUE,
       etage int NOT NULL,
       batiment varchar(20) NOT NULL,
       superficie real NOT NULL,
       capacite_max int NOT NULL,
       carac_tech JSON NOT NULL,
       PRIMARY KEY (nom, etage, batiment),
       FOREIGN KEY (etage, batiment) REFERENCES Etages (num_etage, batiment),
   );
   
   INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
       VALUES ('#13bb79', 1, 'Green', 56, 10, 
           '{
               "id" : 1, 
               "puissance_elec" : 94, 
               "arrivee_elec" : FALSE, 
               "arrivee_gaz" : "Butane", 
               "nb_prises_elec" : 3, 
               "nb_prises_reseaux" : 2
           }'
       );
   ```

4. Nous pouvons également utiliser JSON pour décomposer la table Imgs et utiliser JSON pour représenter l'association de un à un `plan_salle` entre la table `Salles` et la table `Imgs` et aussi pour représenter l'attribut multivalué de photos dans la table de `Salles` et l'attribut `plan_etages` de la table `Etages`

   ```sql
   --modifier trois tables qui ont une association avec la table Imgs
   --Table Etages
   CREATE TABLE Etages (
       num_etage int NOT NULL,
       batiment varchar(20) NOT NULL,
       plan_salles JSON NOT NULL,
       PRIMARY KEY (num_etage, batiment),
       FOREIGN KEY (batiment) REFERENCES Batiments (nom)
   );
   
   INSERT INTO Etages (num_etage, batiment, plan_salles)
       VALUES (1, 'Green','[
               {
                   "url": "https://addthis.com/ultrices/phasellus/id/sapien/in/sapien.xml",
                   "num_etage": 1
               },
               {
                   "url": "https://multiply.com/scelerisque.jpg",
                   "num_etage": 2
               },
               {
                   "url": "https://google.com.br/vestibulum/sagittis.png",
                   "num_etage": 3
               },
               {
                   "url": "https://ca.gov/suscipit/a.js",
                   "num_etage": 4
               },
               {
                   "url": "https://cdbaby.com/eros/vestibulum/ac/est/lacinia.xml",
                   "num_etage": 5
               }
           ]');
           
   ---Table Salles   
   CREATE TABLE Salles (
       nom varchar(20) NOT NULL UNIQUE,
       etage int NOT NULL,
       batiment varchar(20) NOT NULL,
       superficie real NOT NULL,
       capacite_max int NOT NULL,
       carac_tech JSON NOT NULL,
       plan_salle JSON NOT NULL UNIQUE,
       photos JSON,
       PRIMARY KEY (nom, etage, batiment),
       FOREIGN KEY (etage, batiment) REFERENCES Etages (num_etage, batiment),
   );   
      
   INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech, plan_salle, photos)
       VALUES ('#13bb79', 1, 'Green', 56, 10, 
           '{
               "id" : 1, 
               "puissance_elec" : 94, 
               "arrivee_elec" : "FALSE", 
               "arrivee_gaz" : "Butane", 
               "nb_prises_elec" : 3, 
               "nb_prises_reseaux" : 2
           }',
           '{
                   "url": "https://cdbaby.com/eros/vestibulum/ac/est/lacinia.xml"
           }',
           '[
               {
                   "url": "https://addthis.com/ultrices/phasellus/id/sapien/in/sapien.xml"
               },
               {
                   "url": "https://multiply.com/scelerisque.jpg"
               },
               {
                   "url": "https://google.com.br/vestibulum/sagittis.png"
               },
               {
                   "url": "https://ca.gov/suscipit/a.js"
               },
               {
                   "url": "https://cdbaby.com/eros/vestibulum/ac/est/lacinia.xml"
               }
           ]'
    );
    
   ---Table Organisation
   
   CREATE TABLE Organisations (
       sigle varchar(20) PRIMARY KEY,
       nom varchar(50) NOT NULL,
       directeur varchar(20) UNIQUE,
       type varchar(1) NOT NULL CHECK (TYPE = 'L' OR TYPE = 'D'),
       thematique_etude JSON,
       domaine_expertise JSON,
       logo JSON
       FOREIGN KEY (directeur) REFERENCES Employes (num_badge),
       CHECK ((TYPE = 'L' AND thematique_etude is NOT NULL AND domaine_expertise is NULL AND logo IS NOT NULL) OR (TYPE = 'D' AND thematique_etude is NULL AND domaine_expertise is NOT NULL AND logo IS NULL))
   );
   
   INSERT INTO Organisations (sigle, nom, TYPE, thematique_etude, logo)
       VALUES ('ML', 'Microbiologie', 'L', 'Microbiologie',
       '{
           "url": "https://pinterest.com/magnis/dis/parturient/montes/nascetur.xml",
           "serveur": "#a4e308"
               }'
       );
   
   ```

   

## Gestion de droit

Nous avons mis en place trois groupes pour l'ensemble du système:`HR`, `ADMIN`, `SUPPORT`

```sql
CREATE GROUP HR WITH USER hr1, hr2;
CREATE GROUP ADMIN WITH USER admin1, admin2;
CREATE GROUP SUPPORT WITH USER support1, support1;
```

RH: Gérez les employés, consultez le statut actuel de chaque employé et la composition de chaque organisation de l'entreprise. Mais incapable de gérer le matériel de l'entreprise, tel que Machines, indique qu'ils n'ont pas le droit de fonctionner.

ADMIN: a la plus haute autorité sur toutes les tables

SUPPORT: Ne peut gérer que toutes les salles, machines et moyens informatiques de l'entreprise

```sql
GRANT SELECT, UPDATE, INSERT, DELETE ON Employes, Participe, Projets, Taches, Organisations TO HR;
GRANT ALL PRIVILEGES ON gestion_patrimoine TO ADMIN;
GRANT SELECT, UPDATE, INSERT, DELETE ON Salles, Etages, Batiments, Machines, Postes_tele, PC_fixes, PC_portables, PC_serveurs TO SUPPORT;
```





