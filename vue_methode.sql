-- On va concevoir une méthode `employesZombis` pour afficher la liste de `Employes` qui ne sont actuellement dans aucun projet.
CREATE VIEW VemployesZombis AS
SELECT
    e.nom,
    e.prenom,
    e.num_badge
FROM
    Employes AS e
    JOIN (
        SELECT
            num_badge
        FROM
            Employes
    EXCEPT
    SELECT
        employe
    FROM
        Participe) AS num ON e.num_badge = num.num_badge;

SELECT
    *
FROM
    VemployesZombis;

SELECT
    *
FROM
    Participe;

SELECT
    *
FROM
    Employes;

DROP VIEW VemployesZombis;

-- `nbResponMoyensinfo` est utilisé pour afficher une liste qui contient des `Employes` qui sont les responsables des moyens informatiques et ses nombres de moyen informatique.
CREATE VIEW VNbResponMoyensinfo AS
SELECT
    e.nom,
    e.prenom,
    e.num_badge,
    Count(e.num_badge) AS nb_moyens_info
FROM
    Employes AS e
    JOIN VMoyen_Info AS m ON m.responsable = e.num_badge
GROUP BY
    e.num_badge;

DROP VIEW VNbResponMoyensinfo;

SELECT
    *
FROM
    VNbResponMoyensinfo;

SELECT
    *
FROM
    VMoyen_Info;

-- Avec la méthode `directeursSurcharges`, nous pouvons trouver des employés avec plusieurs fonctions. Bien que nous ayons précédemment stipulé que l'entreprise n'autorisait que les employés à diriger au plus une organisation, nous pouvons toujours utiliser cette méthode pour trouver des employés qui dirigent plusieurs organisations.
CREATE VIEW VdirecteursSurcharges AS
SELECT
    e.nom,
    e.prenom,
    e.num_badge,
    COUNT(e.num_badge) AS nb_organisations
FROM
    Employes AS e
    JOIN Organisations AS o ON o.directeur = e.num_badge
GROUP BY
    e.num_badge
HAVING
    Count(e.num_badge) > 0;

DROP VIEW VdirecteursSurcharges;

SELECT
    *
FROM
    VdirecteursSurcharges;

SELECT
    *
FROM
    Employes;

-- Dans le même temps, nous chercherons également des employés qui est le chef de plusieurs projets en même temps à l'aide de la méthode `chefsSurcharges`.
CREATE VIEW VchefsSurcharges AS
SELECT
    e.nom,
    e.prenom,
    e.num_badge,
    COUNT(e.num_badge) AS nb_projets
FROM
    Employes AS e
    JOIN Projets AS p ON p.chef = e.num_badge
GROUP BY
    e.num_badge
HAVING
    Count(e.num_badge) > 0;

DROP VIEW VchefsSurcharges;

SELECT
    *
FROM
    VchefsSurcharges;

SELECT
    *
FROM
    Projets;

-- De plus, nous avons créé la méthode `membresSurcharges` pour rechercher des employés avec plusieurs projets.
CREATE VIEW VmembresSurcharges AS
SELECT
    e.nom,
    e.prenom,
    e.num_badge,
    COUNT(e.num_badge) AS nb_projets
FROM
    Employes AS e
    JOIN Participe AS p ON p.employe = e.num_badge
GROUP BY
    e.num_badge
HAVING
    Count(e.num_badge) > 0;

DROP VIEW VmembresSurcharges;

SELECT
    *
FROM
    VmembresSurcharges;

SELECT
    *
FROM
    Participe;

-- Une méthode `calcuResteSalle` nous permet de calculez la surface restante de chaque salle. Nous pouvons prédire la salle appropriée en fonction de la taille de la machine installée dans chaque salle.
-------------------VcalcuResteSalle-------------------

CREATE VIEW VcalcuResteSalle AS
SELECT
    s.nom,
    s.etage,
    s.batiment,
    s.superficie - SUM(m.taille_machine) AS reste
FROM
    Salles AS s
    JOIN Machines AS m ON m.nom_salle = s.nom
        AND m.etage_salle = s.etage
        AND m.batiment_salle = s.batiment
GROUP BY
    s.nom,
    s.etage,
    s.batiment;

DROP VIEW VcalcuResteSalle;

SELECT
    *
FROM
    VcalcuResteSalle;

SELECT
    *
FROM
    Machines;

-- En plus de calculer la surface restante de la pièce, nous devons également déterminer si le nombre d'autres équipements restant dans la salle est suffisant, nous avons donc conçu des méthodes `calcuPrisesReseaux` et `calcuPrisesElec` pour calculer le nombre de prises restantes de chaque salle.
-------------------VcalcuPrisesReseaux-------------------

CREATE VIEW VcalcuPrisesReseaux AS
SELECT
    s_c.nom,
    s_c.etage,
    s_c.batiment,
    s_c.nb_prises_reseaux - COUNT(m_b.nb_machine) AS reste
FROM (
    SELECT
        *
    FROM
        Salles AS s
        JOIN Carac_tech AS c ON c.id = s.carac_tech) AS s_c
    JOIN (
        SELECT
            CASE b.besoin_reseau
            WHEN TRUE THEN
                1
            END AS nb_machine,
            m.nom_salle,
            m.etage_salle,
            m.batiment_salle
        FROM
            Machines AS m
            JOIN Besoins AS b ON m.modele = b.id) AS m_b ON m_b.nom_salle = s_c.nom
        AND m_b.etage_salle = s_c.etage
        AND m_b.batiment_salle = s_c.batiment
GROUP BY
    s_c.nom,
    s_c.etage,
    s_c.batiment,
    s_c.nb_prises_reseaux;

DROP VIEW VcalcuPrisesReseaux;

SELECT
    *
FROM
    VcalcuPrisesReseaux;

SELECT
    s.nom,
    c.nb_prises_reseaux
FROM
    Salles s
    JOIN carac_tech c ON s.carac_tech = c.id;

SELECT
    *
FROM
    Machines m
    JOIN Besoins b ON m.modele = b.id;

-------------------VcalcuPrisesElec-------------------
CREATE VIEW VcalcuPrisesElec AS
SELECT
    s_c.nom,
    s_c.etage,
    s_c.batiment,
    s_c.nb_prises_elec - COUNT(m_b.nb_machine) AS reste
FROM (
    SELECT
        *
    FROM
        Salles AS s
        JOIN Carac_tech AS c ON c.id = s.carac_tech) AS s_c
    JOIN (
        SELECT
            CASE b.besoin_reseau
            WHEN TRUE THEN
                1
            END AS nb_machine,
            m.nom_salle,
            m.etage_salle,
            m.batiment_salle
        FROM
            Machines AS m
            JOIN Besoins AS b ON m.modele = b.id) AS m_b ON m_b.nom_salle = s_c.nom
        AND m_b.etage_salle = s_c.etage
        AND m_b.batiment_salle = s_c.batiment
GROUP BY
    s_c.nom,
    s_c.etage,
    s_c.batiment,
    s_c.nb_prises_elec;

DROP VIEW VcalcuPrisesElec;

SELECT
    *
FROM
    VcalcuPrisesElec;

SELECT
    s.nom,
    c.nb_prises_elec
FROM
    Salles s
    JOIN carac_tech c ON s.carac_tech = c.id;

SELECT
    *
FROM
    Machines m
    JOIN Besoins b ON m.modele = b.id;


/*-----------------------VUE JSON-----------------------*/
SET search_path TO gestion_patrimoine_json;

CREATE VIEW VSallePhotos AS
SELECT
    s.nom,
    p ->> 'url' AS url
FROM
    Salles s,
    JSON_ARRAY_ELEMENTS(s.photos) p;

CREATE VIEW VBatimentPlansEtage AS
SELECT
    b.nom,
    p ->> 'url' AS url,
    p ->> 'num_etage' AS num_etage
FROM
    Batiments b,
    JSON_ARRAY_ELEMENTS(b.plans_etage) p;

CREATE VIEW VSalleCaracTech AS
SELECT
    s.nom,
    s.carac_tech ->> 'puissance_elec' AS puissance_elec,
    s.carac_tech ->> 'arrivee_elec' AS arrivee_elec,
    s.carac_tech ->> 'arrivee_gaz' AS arrivee_gaz,
    s.carac_tech ->> 'nb_prises_elec' AS nb_prises_elec,
    s.carac_tech ->> 'nb_prises_reseaux' AS nb_prises_reseaux
FROM
    Salles s;

CREATE VIEW VEmployeOrganisations AS
SELECT
    e.nom,
    e.prenom,
    e.num_badge,
    o ->> 'sigle' AS organisation
FROM
    Employes e,
    JSON_ARRAY_ELEMENTS(e.organisation) o;

SELECT
    *
FROM
    VEmployeOrganisations;

