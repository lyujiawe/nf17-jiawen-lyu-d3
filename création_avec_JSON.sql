SET search_path TO gestion_patrimoine_json;
CREATE TABLE Batiments (
    nom varchar(20) PRIMARY KEY,
    superficie real NOT NULL,
    lat FLOAT(8) NOT NULL,
    lng FLOAT(8) NOT NULL,
    plans_etage JSON NOT NULL,
    UNIQUE(lat, lng)
);

INSERT INTO Batiments (nom, superficie, lat, lng)
    VALUES ('Green', 28000, 38.299031, - 8.4830925,
    '[
            {
                "url": "https://addthis.com/ultrices/phasellus/id/sapien/in/sapien.xml",
                "num_etage": 1,
                "serveur": "#a4e308"
            },
            {
                "url": "https://multiply.com/scelerisque.jpg",
                "num_etage": 2,
                "serveur": "#a4e308"
            },
            {
                "url": "https://google.com.br/vestibulum/sagittis.png",
                "num_etage": 3,
                "serveur": "#a4e308"
            },
            {
                "url": "https://ca.gov/suscipit/a.js",
                "num_etage": 4,
                "serveur": "#a4e308"
            },
            {
                "url": "https://cdbaby.com/eros/vestibulum/ac/est/lacinia.xml",
                "num_etage": 5,
                "serveur": "#a4e308"
            }
        ]'
    );
    
CREATE TABLE Etages (
    num_etage int NOT NULL,
    batiment varchar(20) NOT NULL,
    PRIMARY KEY (num_etage, batiment),
    FOREIGN KEY (batiment) REFERENCES Batiments (nom)
);


INSERT INTO Etages (num_etage, batiment)
    VALUES (1, 'Green');

CREATE TABLE Salles (
    nom varchar(20) NOT NULL UNIQUE,
    etage int NOT NULL,
    batiment varchar(20) NOT NULL,
    superficie real NOT NULL,
    capacite_max int NOT NULL,
    carac_tech JSON NOT NULL,
    plan_salle JSON NOT NULL,
    photos JSON,
    PRIMARY KEY (nom, etage, batiment),
    FOREIGN KEY (etage, batiment) REFERENCES Etages (num_etage, batiment)
);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech, plan_salle, photos)
    VALUES ('#13bb79', 1, 'Green', 56, 10, 
        '{
            "id" : 1, 
            "puissance_elec" : 94, 
            "arrivee_elec" : "FALSE", 
            "arrivee_gaz" : "Butane", 
            "nb_prises_elec" : 3, 
            "nb_prises_reseaux" : 2
        }',
        '{
                "url": "https://addthis.com/ultrices/phasellus/id/sapien/in/sapien.xml",
                "num_etage": 1,
                "serveur": "#a4e308"
        }',
        '[
            {
                "url": "https://addthis.com/ultrices/phasellus/id/sapien/in/sapien.xml",
                "serveur": "#a4e308"
            },
            {
                "url": "https://multiply.com/scelerisque.jpg",
                "serveur": "#a4e308"
            },
            {
                "url": "https://google.com.br/vestibulum/sagittis.png",
                "serveur": "#a4e308"
            },
            {
                "url": "https://ca.gov/suscipit/a.js",
                "serveur": "#a4e308"
            },
            {
                "url": "https://cdbaby.com/eros/vestibulum/ac/est/lacinia.xml",
                "serveur": "#a4e308"
            }
        ]'
    );



CREATE TABLE Employes (
    num_badge varchar(20) PRIMARY KEY,
    nom varchar(20) NOT NULL,
    prenom varchar(20) NOT NULL,
    email varchar(50) NOT NULL UNIQUE,
    statut_employe JSON NOT NULL,
    organisation JSON NOT NULL,
    nom_bureau varchar(20) NOT NULL,
    etage_bureau int NOT NULL,
    batiment_bureau varchar(20) NOT NULL,
    FOREIGN KEY (nom_bureau, etage_bureau, batiment_bureau) REFERENCES Salles (nom, etage, batiment)
);

INSERT INTO Employes (num_badge, nom, prenom, email, statut_employe, organisation, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('#2280ef', 'Saltrese', 'Shannan', 'ssaltrese0@pro.jp', 
    '{
        "type": "CDI"
    }', 
    '[
        {
            "sigle":"ML"
        },
        {
            "sigle":"PMD"
        }
    ]', 
    '#13bb79', 1, 'Green'
    );

CREATE TABLE Machines (
    num_contrat varchar(20) PRIMARY KEY,
    modele JSON NOT NULL,
    description text,
    entreprise_maintenance varchar(20),
    taille_machine int NOT NULL,
    nom_salle varchar(20) NOT NULL,
    etage_salle int NOT NULL,
    batiment_salle varchar(20) NOT NULL,
    FOREIGN KEY (nom_salle, etage_salle, batiment_salle) REFERENCES Salles (nom, etage, batiment),
);



CREATE TABLE Postes_tele (
    num_interne varchar(20),
    num_direct varchar(20),
    type varchar(1) NOT NULL CHECK (TYPE = 'V' OR TYPE = 'T' OR TYPE = 'L'),
    modele_tele varchar(20) NOT NULL,
    marque varchar(20) NOT NULL,
    nom_bureau varchar(20) NOT NULL,
    etage_bureau int NOT NULL,
    besoin_reseau boolean,
    batiment_bureau varchar(20) NOT NULL,
    PRIMARY KEY (num_interne, num_direct),
    FOREIGN KEY (nom_bureau, etage_bureau, batiment_bureau) REFERENCES Salles (nom, etage, batiment)
);

CREATE TABLE PC_fixes (
    nom varchar(20) PRIMARY KEY,
    os varchar(10) NOT NULL CHECK (os = 'Linux' OR os = 'Windows' OR os = 'Mac'),
    responsable varchar(20) NOT NULL,
    adresse_MAC varchar(17) NOT NULL UNIQUE,
    adresse_IP varchar(15) NOT NULL UNIQUE,
    nom_salle varchar(20) NOT NULL,
    etage_salle int NOT NULL,
    batiment_salle varchar(20) NOT NULL,
    FOREIGN KEY (nom_salle, etage_salle, batiment_salle) REFERENCES Salles (nom, etage, batiment),
    FOREIGN KEY (responsable) REFERENCES Employes (num_badge)
);

CREATE TABLE PC_portables (
    nom varchar(20) PRIMARY KEY,
    os varchar(10) NOT NULL CHECK (os = 'Linux' OR os = 'Windows' OR os = 'Mac'),
    responsable varchar(20) NOT NULL,
    adresse_MAC varchar(17) NOT NULL UNIQUE,
    adresse_IP varchar(15) NOT NULL UNIQUE,
    nom_salle varchar(20),
    etage_salle int,
    batiment_salle varchar(20),
    ordi_fonction varchar(20),
    FOREIGN KEY (nom_salle, etage_salle, batiment_salle) REFERENCES Salles (nom, etage, batiment),
    FOREIGN KEY (responsable) REFERENCES Employes (num_badge),
    FOREIGN KEY (ordi_fonction) REFERENCES Employes (num_badge),
    CHECK ((ordi_fonction IS NOT NULL AND nom_salle is NULL AND etage_salle is NULL AND batiment_salle is NULL) OR (ordi_fonction is NULL AND nom_salle is NOT NULL AND etage_salle is NOT NULL AND batiment_salle is NOT NULL))
);

CREATE TABLE PC_serveurs (
    nom varchar(20) PRIMARY KEY,
	responsable varchar(20) NOT NULL,
    nom_salle varchar(20) NOT NULL,
    etage_salle int NOT NULL,
    batiment_salle varchar(20) NOT NULL,
    adresse_MAC JSON NOT NULL,
    FOREIGN KEY (nom_salle, etage_salle, batiment_salle) REFERENCES Salles (nom, etage, batiment),
    FOREIGN KEY (responsable) REFERENCES Employes (num_badge)
);


INSERT INTO PC_serveurs (nom, responsable , nom_salle, etage_salle, batiment_salle, adresse_MAC) VALUES (
    '#4fa757',
    '#5a96a6',
    '#13bb79',
    1,
    'Green',
   '[
        {
            "adresse_MAC":"CA-D0-1A-75-77-DB", 
            "adresse_IP": "97.164.67.22"
        },
        {
            "adresse_MAC":"CA-D0-1A-75-77-DB", 
            "adresse_IP": "213.27.10.62"
        }
   ]
    '
);

CREATE TABLE Organisations (
    sigle varchar(20) PRIMARY KEY,
    nom varchar(50) NOT NULL,
    directeur varchar(20) UNIQUE,
    type varchar(1) NOT NULL CHECK (TYPE = 'L' OR TYPE = 'D'),
    thematique_etude JSON,
    domaine_expertise JSON,
    logo JSON
    FOREIGN KEY (directeur) REFERENCES Employes (num_badge),
    CHECK ((TYPE = 'L' AND thematique_etude is NOT NULL AND domaine_expertise is NULL AND logo IS NOT NULL) OR (TYPE = 'D' AND thematique_etude is NULL AND domaine_expertise is NOT NULL AND logo IS NULL))
);

INSERT INTO Organisations (sigle, nom, TYPE, thematique_etude, logo)
    VALUES ('ML', 'Microbiologie', 'L', 'Microbiologie',
    '{
        "url": "https://pinterest.com/magnis/dis/parturient/montes/nascetur.xml",
        "serveur": "#a4e308"
            }'
    );

CREATE TABLE Projets (
    sigle varchar(20) PRIMARY KEY,
    nom varchar NOT NULL UNIQUE,
    description text,
    start_date date NOT NULL,
    end_date date,
    chef varchar(20),
    FOREIGN KEY (chef) REFERENCES Employes (num_badge),
    CHECK(start_date < end_date)
);

CREATE TABLE Taches (
    id serial,
    projet varchar(20),
    start_date date NOT NULL,
    end_date date,
    PRIMARY KEY (id, projet),
    FOREIGN KEY (projet) REFERENCES Projets (sigle),
    CHECK(start_date < end_date)
);

CREATE TABLE Participe (
    employe varchar(20),
    id_tache serial,
    projet varchar(20),
    role varchar(20) NOT NULL,
    FOREIGN KEY (id_tache, projet) REFERENCES Taches (id, projet),
    FOREIGN KEY (employe) REFERENCES Employes (num_badge),
    PRIMARY KEY (employe, id_tache, projet)
);

CREATE TABLE Projet_moyen (
    projet varchar(20),
    moyen_info varchar(20),
    FOREIGN KEY (projet) REFERENCES Projets(sigle),
    FOREIGN KEY (moyen_info) REFERENCES vMoyen_info (nom),
    PRIMARY KEY (projet, moyen_info)
)

CREATE TABLE Org_moyen (
    organisation varchar(20),
    moyen_info varchar(20),
    FOREIGN KEY (organisation) REFERENCES Organisations(sigle),
    FOREIGN KEY (moyen_info) REFERENCES vMoyen_info (nom),
    PRIMARY KEY (organisation, moyen_info)
)

CREATE TABLE Org_machine (
    organisation varchar(20),
    machine varchar(20),
    FOREIGN KEY (organisation) REFERENCES Organisations(sigle),
    FOREIGN KEY (machine) REFERENCES Machines (num_contrat),
    PRIMARY KEY (organisation, machine)
)
