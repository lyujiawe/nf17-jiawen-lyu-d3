CREATE TABLE Batiments (
    nom varchar(20) PRIMARY KEY,
    superficie real NOT NULL,
    lat FLOAT(8) NOT NULL,
    lng FLOAT(8) NOT NULL,
    UNIQUE(lat, lng)
);
CREATE TABLE Etages (
    num_etage int NOT NULL,
    batiment varchar(20) NOT NULL,
    PRIMARY KEY (num_etage, batiment),
    FOREIGN KEY (batiment) REFERENCES Batiments (nom)
);

CREATE TABLE Salles (
    nom varchar(20) NOT NULL UNIQUE,
    etage int NOT NULL,
    batiment varchar(20) NOT NULL,
    superficie real NOT NULL,
    capacite_max int NOT NULL,
    carac_tech serial NOT NULL,
    PRIMARY KEY (nom, etage, batiment),
    FOREIGN KEY (etage, batiment) REFERENCES Etages (num_etage, batiment),
    FOREIGN KEY (carac_tech) REFERENCES Carac_tech (id)
);

CREATE TABLE Carac_tech (
    id serial PRIMARY KEY,
    puissance_elec int NOT NULL,
    arrivee_elec boolean NOT NULL,
    arrivee_gaz varchar NOT NULL,
    nb_prises_elec int NOT NULL,
    nb_prises_reseaux int NOT NULL
);

CREATE TABLE Imgs (
    url text PRIMARY KEY,
    type varchar(2) NOT NULL CHECK (TYPE = 'PE' OR TYPE = 'PS' OR TYPE = 'LO' OR TYPE = 'PH'),
    serveur varchar(20) NOT NULL,
    nom_plan_salle varchar(20),
    etage_plan_salle int,
    batiment_plan_salle varchar(20),
    etage_plan_etage int,
    batiment_plan_etage varchar(20),
    laboratoire varchar(20) UNIQUE,
    FOREIGN KEY (nom_plan_salle, etage_plan_salle, batiment_plan_salle) REFERENCES Salles (nom, etage, batiment),
    FOREIGN KEY (etage_plan_etage, batiment_plan_etage) REFERENCES Etages (num_etage, batiment),
    FOREIGN KEY (laboratoire) REFERENCES Organisations (sigle),
    FOREIGN KEY (serveur) REFERENCES PC_serveurs (nom),
    CHECK (SUBSTR(url, 1, 8) = 'https://'),
    CHECK ((TYPE = 'PH' AND nom_plan_salle IS NOT NULL AND etage_plan_salle IS NOT NULL AND batiment_plan_salle IS NOT NULL AND etage_plan_etage IS NULL AND batiment_plan_etage IS NULL AND laboratoire IS NULL) OR (TYPE = 'PS' AND nom_plan_salle IS NOT NULL AND etage_plan_salle IS NOT NULL AND batiment_plan_salle IS NOT NULL AND etage_plan_etage IS NULL AND batiment_plan_etage IS NULL AND laboratoire IS NULL) OR (TYPE = 'PE' AND nom_plan_salle IS NULL AND etage_plan_salle IS NULL AND batiment_plan_salle IS NULL AND etage_plan_etage IS NOT NULL AND batiment_plan_etage IS NOT NULL AND laboratoire IS NULL) OR (TYPE = 'LO' AND nom_plan_salle IS NULL AND etage_plan_salle IS NULL AND batiment_plan_salle IS NULL AND etage_plan_etage IS NULL AND batiment_plan_etage IS NULL AND laboratoire IS NOT NULL))
);

ALTER TABLE Imgs
    ADD CONSTRAINT fk_imgs_organisations FOREIGN KEY (laboratoire) REFERENCES Organisations (sigle),
	ADD CONSTRAINT fk_imgs_pc_serveurs FOREIGN KEY (serveur) REFERENCES PC_serveurs (nom)
;

CREATE TABLE Employes (
    num_badge varchar(20) PRIMARY KEY,
    nom varchar(20) NOT NULL,
    prenom varchar(20) NOT NULL,
    email varchar(50) NOT NULL UNIQUE,
    statut_employe varchar(20) NOT NULL,
    organisation varchar(20) NOT NULL,
    organisation_secondaire varchar(20),
    nom_bureau varchar(20) NOT NULL,
    etage_bureau int NOT NULL,
    batiment_bureau varchar(20) NOT NULL,
    FOREIGN KEY (nom_bureau, etage_bureau, batiment_bureau) REFERENCES Salles (nom, etage, batiment),
    FOREIGN KEY (statut_employe) REFERENCES Statut_employe (TYPE)
);

ALTER TABLE Employes
    ADD CONSTRAINT fk_employes_organisations FOREIGN KEY (organisation) REFERENCES Organisations (sigle),
	ADD CONSTRAINT fk_employes_organisations_secondaire FOREIGN KEY (organisation_secondaire) REFERENCES Organisations (sigle)
;

CREATE TABLE Statut_employe (
    type varchar(10) PRIMARY KEY
);

CREATE TABLE Machines (
    num_contrat varchar(20) PRIMARY KEY,
    modele serial NOT NULL,
    description text,
    entreprise_maintenance varchar(20),
    taille_machine int NOT NULL,
    nom_salle varchar(20) NOT NULL,
    etage_salle int NOT NULL,
    batiment_salle varchar(20) NOT NULL,
    FOREIGN KEY (nom_salle, etage_salle, batiment_salle) REFERENCES Salles (nom, etage, batiment),
    FOREIGN KEY (modele) REFERENCES Besoins (id)
);

CREATE TABLE Besoins (
    id serial PRIMARY KEY,
    puissance_elec int NOT NULL,
    besoin_triphase boolean NOT NULL,
    besoin_reseau boolean NOT NULL,
    besoin_gaz varchar(20) NOT NULL
);

CREATE TABLE Postes_tele (
    num_interne varchar(20),
    num_direct varchar(20),
    type varchar(1) NOT NULL CHECK (TYPE = 'V' OR TYPE = 'T' OR TYPE = 'L'),
    modele_tele varchar(20) NOT NULL,
    marque varchar(20) NOT NULL,
    nom_bureau varchar(20) NOT NULL,
    etage_bureau int NOT NULL,
    besoin_reseau boolean,
    batiment_bureau varchar(20) NOT NULL,
    PRIMARY KEY (num_interne, num_direct),
    FOREIGN KEY (nom_bureau, etage_bureau, batiment_bureau) REFERENCES Salles (nom, etage, batiment)
);

CREATE TABLE PC_fixes (
    nom varchar(20) PRIMARY KEY,
    os varchar(10) NOT NULL CHECK (os = 'Linux' OR os = 'Windows' OR os = 'Mac'),
    responsable varchar(20) NOT NULL,
    adresse_MAC varchar(17) NOT NULL UNIQUE,
    adresse_IP varchar(15) NOT NULL UNIQUE,
    nom_salle varchar(20) NOT NULL,
    etage_salle int NOT NULL,
    batiment_salle varchar(20) NOT NULL,
    FOREIGN KEY (nom_salle, etage_salle, batiment_salle) REFERENCES Salles (nom, etage, batiment),
    FOREIGN KEY (responsable) REFERENCES Employes (num_badge)
);

CREATE TABLE PC_portables (
    nom varchar(20) PRIMARY KEY,
    os varchar(10) NOT NULL CHECK (os = 'Linux' OR os = 'Windows' OR os = 'Mac'),
    responsable varchar(20) NOT NULL,
    adresse_MAC varchar(17) NOT NULL UNIQUE,
    adresse_IP varchar(15) NOT NULL UNIQUE,
    nom_salle varchar(20),
    etage_salle int,
    batiment_salle varchar(20),
    ordi_fonction varchar(20),
    FOREIGN KEY (nom_salle, etage_salle, batiment_salle) REFERENCES Salles (nom, etage, batiment),
    FOREIGN KEY (responsable) REFERENCES Employes (num_badge),
    FOREIGN KEY (ordi_fonction) REFERENCES Employes (num_badge),
    CHECK ((ordi_fonction IS NOT NULL AND nom_salle is NULL AND etage_salle is NULL AND batiment_salle is NULL) OR (ordi_fonction is NULL AND nom_salle is NOT NULL AND etage_salle is NOT NULL AND batiment_salle is NOT NULL))
);

CREATE TABLE PC_serveurs (
    nom varchar(20) PRIMARY KEY,
	responsable varchar(20) NOT NULL,
    nom_salle varchar(20) NOT NULL,
    etage_salle int NOT NULL,
    batiment_salle varchar(20) NOT NULL,
    FOREIGN KEY (nom_salle, etage_salle, batiment_salle) REFERENCES Salles (nom, etage, batiment),
    FOREIGN KEY (responsable) REFERENCES Employes (num_badge)
);

CREATE TABLE Adresse_MAC_serveur (
    adresse_MAC varchar(17) PRIMARY KEY,
    serveur varchar(20) NOT NULL,
    FOREIGN KEY (serveur) REFERENCES PC_serveurs (nom)
);

CREATE TABLE Adresse_IP_serveur (
    adresse_IP varchar(15) PRIMARY KEY,
    adresse_MAC varchar(17) NOT NULL,
    FOREIGN KEY (adresse_MAC) REFERENCES Adresse_MAC_serveur (adresse_MAC)
);

CREATE TABLE Organisations (
    sigle varchar(20) PRIMARY KEY,
    nom varchar(50) NOT NULL,
    directeur varchar(20) UNIQUE,
    type varchar(1) NOT NULL CHECK (TYPE = 'L' OR TYPE = 'D'),
    thematique_etude varchar(50),
    domaine_expertise varchar(50),
    FOREIGN KEY (directeur) REFERENCES Employes (num_badge),
    FOREIGN KEY (thematique_etude) REFERENCES Thematiques_etude (TYPE),
    FOREIGN KEY (domaine_expertise) REFERENCES Domaines_expertise (TYPE),
    CHECK ((TYPE = 'L' AND thematique_etude is NOT NULL AND domaine_expertise is NULL) OR (TYPE = 'D' AND thematique_etude is NULL AND domaine_expertise is NOT NULL))
);

CREATE TABLE Thematiques_etude (
    type varchar(50) PRIMARY KEY
);

CREATE TABLE Domaines_expertise (
    type varchar(50) PRIMARY KEY
);

CREATE TABLE Projets (
    sigle varchar(20) PRIMARY KEY,
    nom varchar NOT NULL UNIQUE,
    description text,
    start_date date NOT NULL,
    end_date date,
    chef varchar(20),
    FOREIGN KEY (chef) REFERENCES Employes (num_badge),
    CHECK(start_date < end_date)
);

CREATE TABLE Taches (
    id serial,
    projet varchar(20),
    start_date date NOT NULL,
    end_date date,
    PRIMARY KEY (id, projet),
    FOREIGN KEY (projet) REFERENCES Projets (sigle),
    CHECK(start_date < end_date)
);

CREATE TABLE Participe (
    employe varchar(20),
    id_tache serial,
    projet varchar(20),
    role varchar(20) NOT NULL,
    FOREIGN KEY (id_tache, projet) REFERENCES Taches (id, projet),
    FOREIGN KEY (employe) REFERENCES Employes (num_badge),
    PRIMARY KEY (employe, id_tache, projet)
);

CREATE TABLE Projet_moyen (
    projet varchar(20),
    moyen_info varchar(20),
    FOREIGN KEY (projet) REFERENCES Projets(sigle),
    FOREIGN KEY (moyen_info) REFERENCES vMoyen_info (nom),
    PRIMARY KEY (projet, moyen_info)
)

CREATE TABLE Org_moyen (
    organisation varchar(20),
    moyen_info varchar(20),
    FOREIGN KEY (organisation) REFERENCES Organisations(sigle),
    FOREIGN KEY (moyen_info) REFERENCES vMoyen_info (nom),
    PRIMARY KEY (organisation, moyen_info)
)

CREATE TABLE Org_machine (
    organisation varchar(20),
    machine varchar(20),
    FOREIGN KEY (organisation) REFERENCES Organisations(sigle),
    FOREIGN KEY (machine) REFERENCES Machines (num_contrat),
    PRIMARY KEY (organisation, machine)
)
