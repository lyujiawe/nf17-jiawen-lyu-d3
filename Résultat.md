# Résultats pour des méthodes

> Quelles sont les personnes qui ne sont actuellement dans aucun projet ? (Comme toute entreprise, nous cherchons à réduire nos coûts)

* On va concevoir une méthode `employesZombis` pour afficher la liste de `Employes` qui ne sont actuellement dans aucun projet.

  ![Imgur](https://i.imgur.com/fMigsmg.png)

  Ici, je montre l'état de la requête de `VemployesZombis`, `participe`, `Employes`. Nous pouvons voir que tous les employés affichés dans `VemployesZombis` n'ont participé à aucun projet.

> Quelles personnes sont responsables de nombreux moyens informatiques ? (Il serait souhaitable d'éviter qu'une personne soit irremplaçable)

* `nbResponMoyensinfo` est utilisé pour afficher une liste qui contient des `Employes` qui sont les responsables des moyens informatiques et ses nombres de moyen informatique.

  ![Imgur](https://i.imgur.com/yYrvprI.png)

  Nous pouvons envoyer le résultat d'affichage de `VMoyen_Info` pour voir clairement quel employé est responsable de chaque Moyen informatique, et `VNbResponMoyeninfo` peut partir de ʻEmployes` et calculer le nombre de Moyen informatiques dont ils sont actuellement responsables.

> Qui cumule les postes de direction / responsable ? (Nous cherchons à prévenir le surmenage dans notre entreprise, des employés *zombis* ne nous intéressent pas).

* Avec la méthode `directeursSurcharges`, nous pouvons trouver des employés avec plusieurs fonctions. Bien que nous ayons précédemment stipulé que l'entreprise n'autorisait que les employés à diriger au plus une organisation, nous pouvons toujours utiliser cette méthode pour trouver des employés qui dirigent plusieurs organisations. 

  ![Imgur](https://i.imgur.com/Gb8teGu.png)

  Puisque nous avons défini l'attribut `directeur` dans le tableau` Organisations` sur UNIQUE, la valeur de `nb_organisations` est ici un maximum de 1.

* Dans le même temps, nous chercherons également des employés qui est le chef de plusieurs projets en même temps à l'aide de la méthode `chefsSurcharges`.

  ![Imgur](https://i.imgur.com/3HnAtAz.png)

* De plus, nous avons créé la méthode `membresSurcharges` pour rechercher des employés avec plusieurs projets.

  ![Imgur](https://i.imgur.com/mBYqAAm.png)

> Avons-nous des salles en sur-effectif ? (La sécurité est un soucis prioritaire sur nos installations)

* Une méthode `calcuResteSalle` nous permet de calculez la surface restante de chaque salle. Nous pouvons prédire la salle appropriée en fonction de la taille de la machine installée dans chaque salle.

  ![Imgur](https://i.imgur.com/RrizEoL.png)

* En plus de calculer la surface restante de la pièce, nous devons également déterminer si le nombre d'autres équipements restant dans la salle est suffisant, nous avons donc conçu des méthodes `calcuPrisesReseaux` et `calcuPrisesElec` pour calculer le nombre de prises restantes de chaque salle.

  ![Imgur](https://i.imgur.com/U1q77F4.png)

Nous pouvons voir qu'un total de 5 premiers de réseaux sont fournis dans la salle # 58366f. Actuellement, il y a deux machines dans cette salle qui doivent être connectées à Internet, vous pouvez donc voir dans `VcalcuPrisesReseaux` qu'il reste 3 prises de réseaux.

La même situation avec `VcalcuPrisesElec`.

![image-20200612163114026](/Users/haida/Library/Application Support/typora-user-images/image-20200612163114026.png)

# Résultats pour des contraintes

### Affichage de tous des moyens informatiques

<img src="https://i.imgur.com/baqkEXa.png" alt="Imgur" style="zoom:50%;" />

### Affichage de tous des directeurs

![Imgur](https://i.imgur.com/4Rm9r2W.png)

### Affichage de tous des membres

![Imgur](https://i.imgur.com/dLRe3uk.png)



### Contrainte entre Projets et Taches

Dans nos contraintes, la plupart des dispositions de la plupart des contraintes doivent être vides, ici nous testons contre `VstartDateContrainte`

![Imgur](https://i.imgur.com/0Lu0QeX.png)

Ici, nous avons délibérément créé des données erronées. Il peut être constaté que la date de début de la tâche 1 du projet EUR est inférieure à celle de début du projet auquel elle appartient, donc dans `VstartDateContrainte` nous afficherons ce mauvais projet

# Résultats pour vérifier des attributs JSON

### Affichage de tous des plans d'étage pour un bâtiment

<img src="https://i.imgur.com/PR09mk2.png" alt="Imgur" style="zoom: 33%;" />

### Affichage de toutes les organisations auxquelles un employé participe

<img src="https://i.imgur.com/OYqXq6K.png" alt="Imgur" style="zoom: 33%;" />

### Affichage de la table de caractère technique

<img src="https://i.imgur.com/b0cKdRE.png" alt="Imgur" style="zoom: 33%;" />

### Afficher toutes les photos appartenant à une salle

<img src="https://i.imgur.com/VfQK0xt.png" alt="Imgur" style="zoom: 33%;" />