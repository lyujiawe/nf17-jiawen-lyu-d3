# Outil de gestion du patrimoine

## Note de clarification

## Objectifs du document

Il s'agit ici de réaliser un systeme de gestion du patrimoine. Après avoir rappelé les livrables attendus pour ce projet et les besoins établis dans le cahier des charges, nous listerons les objets qui seront gérés dans notre base de données,  avec leurs propriétés et leurs contraintes. Nous listerons ensuite les utilisateurs qui pourront interagir avec cette base de données puis nous établirons les fonctions utilisables par ces derniers.

### Rappel des besoins établis dans le cahier des charges

Nous voulons pouvoir savoir à tout moment l'état de notre entreprise, pour cela un certain nombre de recherches sont souhaitables :

- Quelles sont les personnes qui ne sont actuellement dans aucun projet ? (Comme toute entreprise, nous cherchons à réduire nos coûts)
- Quelles personnes sont responsables de nombreux moyens informatiques ? (Il serait souhaitable d'éviter qu'une personne soit irremplaçable)
- Qui cumule les postes de direction / responsable ? (Nous cherchons à prévenir le surmenage dans notre entreprise, des employés *zombis* ne nous intéressent pas).
- Avons-nous des salles en sur-effectif ? (La sécurité est un soucis prioritaire sur nos installations)

## Description des Objets, de leurs propriétés et des contraintes associées

#### Bâtiments

| Attributs        | Contraintes         |
| :--------------- | :------------------ |
| nom              | unique, not null    |
| superficie       | not null            |
| localisation GPS | unique, not null    |
| nb_etage         | not null, default 1 |

#### Etages

| Attributs         | Contraintes |
| ----------------- | ----------- |
| num_etage         | not null    |
| plans de bâtiment | not null    |

Afin de montrer qu'il existe un plan de bâtiment à chaque étage du bâtiment, et qu'il peut satisfaire au 1NF (les attributs ne peuvent pas être subdivisés) lors de la description de l'adresse du bureau de l'employe, nous avons construit une classe Etages entre `Salles` et `Bâtiments`. `Salles` composent `Etages` et `Etages` composent `Bâtiments`. C'est aussi plus clair logiquement.

#### Salles

| Attributs                   | Contraintes      |
| --------------------------- | ---------------- |
| nom                         | unique, not null |
| superficie                  | not null         |
| Capacité humaine maximale   | not null         |
| photos                      |                  |
| Plan de salle               | not null         |
| caractéristiques techniques | not null         |

Les contraintes de cardinalité que nous stipulons tous sont de 1: 1..N. Parce que chaque bâtiment a un étage, chaque étage a au moins une pièce.



##### Contrainte compliquée :



#### Caractéristiques techniques

| Attributs                    | Contraintes |
| ---------------------------- | ----------- |
| puissance_elec               |             |
| arrivée électrique triphasée |             |
| arrivée air comprimé         |             |
| arrivée gaz spécifique       |             |
| nombre de prises électriques |             |
| nombre de prises réseaux     |             |

Pour toutes les propriétés du tableau `Caractéristiques techniques`, il faut le mettre à non nul pour trouver un local le mieux adapté à cette machine.

-------------------------

#### Imgs

| Attributs | Contraintes                            |
| --------- | -------------------------------------- |
| URL       | not null, unique                       |
| type      | plan_batiment, plan_salle, logo, photo |

Pour les différentes images des Bâtiments ci-dessus, nous avons ici créé un tableau d'images. Évidemment, nous devons utiliser `héritage par la classe mère` pour représenter différents types d'images.

* Les contraintes de cardinalité entre `Plan` et` Salle` et entre `Plan` et ` Etages` est de 1: 1
* La contrainte de cardinalité `Photo` et` Salle` est *: 1. Il ne peut pas y avoir de photos dans une pièce, mais il peut y avoir plusieurs photos. Mais une photo ne peut décrire qu'une seule pièce.

##### Contrainte compliquée 

1. Intersection (Projection (Animal, nom), Projection (Plante, nom)) = Ø

-----------------

#### Employes

| Attributs                    | Contraintes                 |
| ---------------------------- | --------------------------- |
| nom                          | not null                    |
| prenom                       | not null                    |
| numéro de badge              | not null, unique            |
| email                        | not null, unique            |
| statut employe               | (CDI, CDD, Stagiaire, etc.) |
| emplacement du bureau(salle) | not null                    |
| organisation                 | not null                    |
| organisation_secondaire      |                             |

* Nous n'avons pas choisi l'héritage pour les employés, car il y a une situation où un employé est à la fois un leader et un membre d'un autre groupe, donc l'employé est pas héritage non exclusif, et l'employé doit également participer ou être responsable du projet. À ce stade, les objets de directeur et de membre perdent leur sens. Dans ce cas, si l'employé n'est pas une classe abstraite, il est plus pratique d'utiliser la description associée.

* L'association entre `Employes` et `Salles` doit être *: 1, car il est possible que une salle n'est pas utilisée comme bureau d'un employé, ou il peut s'agir d'un nouveau bureau. Bien sûr, dans des circonstances normales, un bureau Il peut y avoir plusieurs employés. Afin d'utiliser raisonnablement les ressources du bureau, nous stipulons qu'un employé ne peut avoir qu'un seul bureau.

* `organisation` et `organisation_secondaire` vont être expliqués dans la partie de `Organisations`

  

##### Contrainte compliquée 

##### Vue

1. vDirecteur = Projection (Jointure(Employes, Organisations, E.num_badge = O.directeur), E)
2. vMembre = Projection()

-------------------------

#### Machines de fabrication et delaboratoire

| Attributs                  | Contraintes      |
| -------------------------- | ---------------- |
| modèle                     | unique, not null |
| description                |                  |
| puissance électrique       |                  |
| besoin du triphasé         |                  |
| besoin réseau              |                  |
| numéro contrat maintenance | unqiue, not null |
| entreprise de maintenance  | not null         |
| besoin d'un gaz spécifique |                  |
| taille de la machine       | not null         |

Nous avons empaqueté puissance électrique, besoin du triphasé, besoin réseau, besoin d'un gaz spécifique dans une autre classe `Besoins`, car `Besoins` peut être déterminé par `modèle` dans `Machines`.

* L'association entre `Machines` et `Salles` est *: 1. Une salle peut accueillir plusieurs machines ou pas de machines, mais une machine doit être placée dans une salle, et nous avons besoin des caractéristiques techniques de la salle pour répondre aux besoins de ces machines.



##### Contrainte compliquée :

---------------------

#### Postes téléphoniques

| Attributs           | Contraintes      |
| ------------------- | ---------------- |
| numéro interne      | unique, not null |
| numéro direct       |                  |
| VOIP/TOIP/Landline  | unique, not null |
| modèle de téléphone | unique, not null |
| marque              | not null, unique |
| bureau              | not null         |

* Les Postes téléphoniques ayant trois types de VOIP / TOIP / Fixe, nous choisissons d'utiliser l'héritage. Comme les attributs entre les différentes catégories sont exactement les mêmes, nous choisissons héritage par la classe mère

* À propos de la connexion entre le `Postes_tele` et `Salles`

   Une salle peut ne pas être équipée d'un téléphone, mais un téléphone ne doit être que dans une seule pièce, donc la limite de nombre est `Postes_tele` *: 1 ` Salles`.

   Nous devons tenir compte de l'équipement réseau de la pièce lorsque nous équipons la pièce d'un téléphone. Évidemment, pour VOIP et TOIP, ces deux types de téléphones nécessitent Internet.

##### Contrainte compliquée :

-------------------------

#### Moyens informatiques

| Attributs     | Contraintes                     |
| ------------- | ------------------------------- |
| nom           | unique, not null                |
| type          | PC fixes, portables ou serveurs |
| os            | Linux, Windows, Mac             |
| adresses MAC  | peut-être plusieur              |
| responsable   | not null, unique                |
| adresses IP   | peut-être plusieur              |
| machine       |                                 |
| projet        |                                 |
| ordi_fonction | unique                          |

* Afin d'exprimer différents types de `Moyens informatiques`, nous avons choisi d'utiliser héritage par les classe fils. Parce que ses trois enfants ont leurs propres associations uniques avec d'autres tables, les associations spécifiques sont:

  * L'association `est dans` de `PC_fixes` et` Salles` est *: 1. C'est la même exigence pour les `PC_servers`.

  * `PC_portables` n'a pas besoin de considérer la question de `Salles`, mais doit considérer la relation avec `Employes`.

  * Pour l'attribut `ordinateur de fonction`, la société stipule que seuls `PC_portables` peuvent être utilisés comme `ordinateur de fonction`

  * Pour les adresses MAC / IP, nous stipulons que pour `PC_fixs` et `PC_portables`, il n'y en a qu'une, mais pour `PC_servers`, la société peut être équipée d'un serveur proxy, donc le serveur peut avoir plusieurs adresses.

    On peut voir que si vous utilisez héritage par la classe mère, cela causera trop de contraintes complexes, nous utilisons donc héritage par les classe fils.

* Nous stipulons que chaque `Moyens informatiques` de l'entreprise doit désigner un `employé` du service maintenance pour la maintenance

* Un `projet` ne peut pas utiliser ou utiliser plusieurs `Moyens informatiques`. Bien entendu, nous permettons également qu'un `Moyens informatiques` soit utilisé par plusieurs `Projets`, car les employés qui utilisent cet ordinateur peuvent avoir de nombreux projets. Cependant, l'association entre «Machines» et «Projets» à Moyens informatiques est similaire, donc la contrainte de cardinalité entre eux est *: *.



##### Contrainte compliquée :

<!--CC1 : Intersection (Projection (Animal, nom), Projection (Plante, nom)) = Ø-->

-------------------------

#### Laboratoires

| Attributs           | Contraintes                                                |
| ------------------- | ---------------------------------------------------------- |
| sigle               | unique, not null                                           |
| directeur           | unique                                                     |
| membres             | on ne peut être membre que d'un seul Laboratoire à la fois |
| logo                | not null, unique                                           |
| nom                 | not null, unique                                           |
| thématiques d'étude | Microbiologie, Manipulation génétique, etc.                |



#### Départements

| Attributs            | Contraintes                                                |
| -------------------- | ---------------------------------------------------------- |
| sigle                | unique, not null                                           |
| nom                  | not null. unique                                           |
| directeur            | unqiue                                                     |
| membres              | on ne peut être membre que d'un seul département à la fois |
| domaines d'expertise | Microbiologie, Manipulation génétique                      |

* Nous pouvons constater que la plupart des attributs des `Laboratoires` et des `Départements` sont les mêmes, donc ici nous laissons ces deux objets hériter d'une Organisation de classe abstraite. Ceci est un héritage presque complet
* Nous autorisons le chef de file à être annulé. Si un employé démissionne et se trouve être le chef de laboratoire, cette fois, le chef de file sera nul. À ce moment, nous pouvons utiliser notre système de gestion pour trouver un successeur approprié. Et afin de ne pas laisser les dirigeants surcharger, nous autorisons un seul employé à diriger au plus une organisation.
* Nous stipulons qu'un employé ne peut être que le chef d'une organisation. Par conséquent, l'association de `est directeur de` entre `Employes` et `Organisations` est de 1: 0..1
* Un employé ne peut rejoindre qu'un département ou un laboratoire au maximum, mais nous autorisons un employé à rejoindre un département et un laboratoire en même temps. Il existe donc une association *: 0..1 appelée `appartient à`entre `Employés` et `Départements`, `Laboratoires`. Dans le même temps, afin d'empêcher l'apparition d'employés sans abri, nous exigerons que l'attribut de `organisation` dans la table `Employes` est `not null`.
* Afin de permettre aux employés de participer à un autre type d'organisation, nous avons ajouté un attribut `organisation_secondaire`.

##### Contrainte compliquée :

---------------------------------

#### Projets

| Attributs   | Contraintes                        |
| ----------- | ---------------------------------- |
| sigle       | unique, not null                   |
| acteurs     | Chaque acteur ayant un rôle défini |
| start_date  | not null                           |
| end_date    | not null                           |
| description |                                    |
| chef        | unique                             |
| nom         | not null, unique                   |

* Afin de mieux répartir les `Employes`, nous avons démonté `Projets` en différentes `Taches`, car le cycle d'un projet peut être long. Nous n'avons aucune raison de laisser les employés ne lui assigner d'autres projets en raison d'un projet d'un an.

  * Il n'y a que l'association `chef de projet` entre le projet et les `Employes`, et c'est 0..1: *. La raisons de 0..1 est similaire à celle de l'association entre `Organisations` et `Employes`. Nous permettons à un employé d'être responsable de plusieurs projets.
* L'association de `Participation` entre `Employes` et `Taches` est *: *, nous la représentons ici avec une classe d'association.

##### Contrainte compliquée :

1. 

-----------------------------

### Méthode à concevoir

Nous devrons pouvoir ***rechercher***, ***ajouter***, ***mettre à jour***, ***supprimer*** des données concernant tous des objets.

Ce système cible principalement deux types d'utilisateurs

* L'un est le personnel de maintenance technique de l'entreprise, car il doit gérer et entretenir les différents instruments de l'entreprise et l'utilisation de différentes salles. 
* L'autre est la direction de l'entreprise, ils doivent utiliser notre système pour assigner raisonnablement des tâches aux employés.

> Quelles sont les personnes qui ne sont actuellement dans aucun projet ? (Comme toute entreprise, nous cherchons à réduire nos coûts)
>

* On va concevoir une méthode `employesZombis` pour afficher la liste de `Employes` qui ne sont actuellement dans aucun projet.
* L'autorisation de cette fonction n'est ouverte qu'à la direction.

> Quelles personnes sont responsables de nombreux moyens informatiques ? (Il serait souhaitable d'éviter qu'une personne soit irremplaçable)

* `nbResponMoyensinfo` est utilisé pour afficher une liste qui contient des `Employes` qui sont les responsables des moyens informatiques et ses nombres de moyen informatique.

* L'autorisation de cette fonction n'est ouverte qu'à le département de maintenance.

> Qui cumule les postes de direction / responsable ? (Nous cherchons à prévenir le surmenage dans notre entreprise, des employés *zombis* ne nous intéressent pas).

* Avec la méthode `directeursSurcharges`, nous pouvons trouver des employés avec plusieurs fonctions. Bien que nous ayons précédemment stipulé que l'entreprise n'autorisait que les employés à diriger au plus une organisation, nous pouvons toujours utiliser cette méthode pour trouver des employés qui dirigent plusieurs organisations. 

* Dans le même temps, nous chercherons également des employés qui est le chef de plusieurs projets en même temps à l'aide de la méthode `chefsSurcharges`.

* De plus, nous avons créé la méthode `membresSurcharges` pour rechercher des employés avec plusieurs projets.

* L'autorisation de cette fonction n'est ouverte qu'à la direction.

> Avons-nous des salles en sur-effectif ? (La sécurité est un soucis prioritaire sur nos installations)

* Une méthode `calcuResteSalle` nous permet de calculez la surface restante de chaque salle. Nous pouvons prédire la salle appropriée en fonction de la taille de la machine installée dans chaque salle.
* En plus de calculer la surface restante de la pièce, nous devons également déterminer si le nombre d'autres équipements restant dans la salle est suffisant, nous avons donc conçu des méthodes `calcuPrisesReseaux` et `calcuPrisesElec` pour calculer le nombre de prises restantes de chaque salle.
* L'autorisation de cette fonction n'est ouverte qu'à le département de maintenance.

