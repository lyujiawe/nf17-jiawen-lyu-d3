---------------------------------Batiments---------------------------------
INSERT INTO Batiments (nom, superficie, lat, lng)
    VALUES ('Green', 28000, 38.299031, - 8.4830925);

INSERT INTO Batiments (nom, superficie, lat, lng)
    VALUES ('Pink', 24899, - 7.7823871, 110.374855);

INSERT INTO Batiments (nom, superficie, lat, lng)
    VALUES ('Turquoise', 25623, 33.9352916, 133.0604733);

INSERT INTO Batiments (nom, superficie, lat, lng)
    VALUES ('Orange', 21287, 39.7927635, 116.3582344);

INSERT INTO Batiments (nom, superficie, lat, lng)
    VALUES ('Red', 25272, 55.8824, 37.489685);

---------------------------------Etages---------------------------------
INSERT INTO Etages (num_etage, batiment)
    VALUES (1, 'Green');

INSERT INTO Etages (num_etage, batiment)
    VALUES (2, 'Green');

INSERT INTO Etages (num_etage, batiment)
    VALUES (3, 'Green');

INSERT INTO Etages (num_etage, batiment)
    VALUES (4, 'Green');

INSERT INTO Etages (num_etage, batiment)
    VALUES (5, 'Green');

------------------
INSERT INTO Etages (num_etage, batiment)
    VALUES (1, 'Pink');

INSERT INTO Etages (num_etage, batiment)
    VALUES (2, 'Pink');

INSERT INTO Etages (num_etage, batiment)
    VALUES (3, 'Pink');

INSERT INTO Etages (num_etage, batiment)
    VALUES (4, 'Pink');

INSERT INTO Etages (num_etage, batiment)
    VALUES (5, 'Pink');

INSERT INTO Etages (num_etage, batiment)
    VALUES (6, 'Pink');

---------------------------------Salles---------------------------------
INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#13bb79', 1, 'Green', 56, 10, 1);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#1a41ce', 1, 'Green', 89, 15, 1);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#8bcab5', 2, 'Green', 56, 15, 2);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#86b429', 2, 'Green', 66, 11, 3);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#369bbd', 3, 'Green', 81, 17, 4);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#5b3eab', 3, 'Green', 84, 15, 5);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#3582a8', 4, 'Green', 53, 20, 1);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#97ec6e', 4, 'Green', 52, 11, 2);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#872472', 5, 'Green', 62, 16, 3);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#0c81f0', 5, 'Green', 57, 16, 4);

------------------
INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#62d26c', 1, 'Pink', 58, 14, 2);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#d6daef', 1, 'Pink', 72, 13, 5);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#c951ef', 2, 'Pink', 71, 11, 1);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#57f26c', 2, 'Pink', 71, 16, 2);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#1244cf', 3, 'Pink', 53, 10, 3);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#b8af36', 3, 'Pink', 93, 17, 4);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#da363b', 4, 'Pink', 93, 18, 5);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#876cd0', 4, 'Pink', 82, 18, 1);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#9b8400', 5, 'Pink', 88, 16, 2);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#8a0e59', 5, 'Pink', 58, 11, 3);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#7320a7', 6, 'Pink', 74, 12, 4);

INSERT INTO Salles (nom, etage, batiment, superficie, capacite_max, carac_tech)
    VALUES ('#58366f', 6, 'Pink', 74, 13, 5);

---------------------------------Carac_tech---------------------------------
INSERT INTO Carac_tech (id, puissance_elec, arrivee_elec, arrivee_gaz, nb_prises_elec, nb_prises_reseaux)
    VALUES (1, 94, FALSE, 'Butane', 3, 2);

INSERT INTO Carac_tech (id, puissance_elec, arrivee_elec, arrivee_gaz, nb_prises_elec, nb_prises_reseaux)
    VALUES (2, 59, FALSE, 'Méthane', 3, 5);

INSERT INTO Carac_tech (id, puissance_elec, arrivee_elec, arrivee_gaz, nb_prises_elec, nb_prises_reseaux)
    VALUES (3, 98, TRUE, 'O2', 2, 4);

INSERT INTO Carac_tech (id, puissance_elec, arrivee_elec, arrivee_gaz, nb_prises_elec, nb_prises_reseaux)
    VALUES (4, 96, TRUE, 'Azote', 8, 7);

INSERT INTO Carac_tech (id, puissance_elec, arrivee_elec, arrivee_gaz, nb_prises_elec, nb_prises_reseaux)
    VALUES (5, 63, FALSE, 'Butane', 1, 2);

---------------------------------Imgs---------------------------------
----plan etage

INSERT INTO Imgs (url, TYPE, serveur, etage_plan_etage, batiment_plan_etage)
    VALUES ('https://addthis.com/ultrices/phasellus/id/sapien/in/sapien.xml', 'PE', '#4fa757', 1, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, etage_plan_etage, batiment_plan_etage)
    VALUES ('https://multiply.com/scelerisque.jpg', 'PE', '#4fa757', 2, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, etage_plan_etage, batiment_plan_etage)
    VALUES ('https://google.com.br/vestibulum/sagittis.png', 'PE', '#4fa757', 3, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, etage_plan_etage, batiment_plan_etage)
    VALUES ('https://ca.gov/suscipit/a.js', 'PE', '#4fa757', 4, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, etage_plan_etage, batiment_plan_etage)
    VALUES ('https://cdbaby.com/eros/vestibulum/ac/est/lacinia.xml', 'PE', '#4fa757', 5, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, etage_plan_etage, batiment_plan_etage)
    VALUES ('https://rediff.com/duis/bibendum.jsp', 'PE', '#4fa757', 1, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, etage_plan_etage, batiment_plan_etage)
    VALUES ('https://sina.com.cn/dolor/sit/amet/consectetuer/adipiscing.jpg', 'PE', '#4fa757', 2, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, etage_plan_etage, batiment_plan_etage)
    VALUES ('https://sakura.ne.jp/varius/ut/blandit/non/interdum/in.aspx', 'PE', '#4fa757', 3, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, etage_plan_etage, batiment_plan_etage)
    VALUES ('https://weebly.com/quisque/id/justo/sit.jsp', 'PE', '#4fa757', 4, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, etage_plan_etage, batiment_plan_etage)
    VALUES ('https://cisco.com/dictumst/maecenas/ut.jpg', 'PE', '#4fa757', 5, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, etage_plan_etage, batiment_plan_etage)
    VALUES ('https://ameblo.jp/viverra.html', 'PE', '#4fa757', 6, 'Pink');

----plan salle
INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://dailymail.co.uk/enim/sit/amet/nunc/viverra.html', 'PS', '#a4e308', '#13bb79', 1, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://google.co.jp/in/felis/donec/semper/sapien/a.js', 'PS', '#a4e308', '#1a41ce', 1, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://cdc.gov/erat/volutpat/in/congue.html', 'PS', '#a4e308', '#8bcab5', 2, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://is.gd/fusce/congue.js', 'PS', '#a4e308', '#86b429', 2, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://howstuffworks.com/habitasse/platea/dictumst/aliquam/augue/quam/sollicitudin.js', 'PS', '#a4e308', '#369bbd', 3, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://boston.com/ut.json', 'PS', '#a4e308', '#5b3eab', 3, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://hc360.com/amet/justo.png', 'PS', '#a4e308', '#3582a8', 4, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://blogtalkradio.com/tellus/nulla/ut/erat/id/mauris/vulputate.png', 'PS', '#a4e308', '#97ec6e', 4, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://jimdo.com/cum.png', 'PS', '#a4e308', '#872472', 5, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('http://myspace.com/tristique/tortor/eu.json', 'PS', '#a4e308', '#0c81f0', 5, 'Green');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://illinois.edu/ut/suscipit/a.json', 'PS', '#a4e308', '#62d26c', 1, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://howstuffworks.com/odio/elementum/eu.html', 'PS', '#a4e308', '#d6daef', 1, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://nyu.edu/ac/est.js', 'PS', '#a4e308', '#c951ef', 2, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://newsvine.com/amet/justo/morbi/ut/odio/cras/mi.png', 'PS', '#a4e308', '#57f26c', 2, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://wisc.edu/turpis/sed/ante/vivamus.js', 'PS', '#a4e308', '#1244cf', 3, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://cpanel.net/feugiat/non.png', 'PS', '#a4e308', '#b8af36', 3, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://list-manage.com/integer/tincidunt/ante.js', 'PS', '#a4e308', '#da363b', 4, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://ameblo.jp/odio/curabitur/convallis.html', 'PS', '#a4e308', '#876cd0', 4, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://com.com/primis/in.jsp', 'PS', '#a4e308', '#7320a7', 6, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://slashdot.org/sed/tincidunt/eu.xml', 'PS', '#a4e308', '#9b8400', 5, 'Pink');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle)
    VALUES ('https://google.ru/at/nibh/in/hac.jsp', 'PS', '#a4e308', '#8a0e59', 5, 'Pink');

----logo
INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle, etage_plan_etage, batiment_plan_etage, laboratoire)
    VALUES ('http://dailymail.co.uk/mi/integer/ac/neque/duis/bibendum.js', 'LO', '#5a065a', '#67b9ac', 5, 'Blue', 2, 'Blue', 'Georgy');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle, etage_plan_etage, batiment_plan_etage, laboratoire)
    VALUES ('http://zimbio.com/morbi/porttitor/lorem/id/ligula/suspendisse.aspx', 'Monique', '#0ac0b7', '#23d3ad', 1, 'Turquoise', 3, 'Red', 'Doyle');

----photo
INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle, etage_plan_etage, batiment_plan_etage, laboratoire)
    VALUES ('https://skyrock.com/in/hac/habitasse.aspx', 'Larry', '#afa339', '#5d02eb', 4, 'Pink', 1, 'Goldenrod', 'Aretha');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle, etage_plan_etage, batiment_plan_etage, laboratoire)
    VALUES ('http://wix.com/amet/lobortis/sapien/sapien/non.png', 'Hatti', '#e4e103', '#0427e3', 2, 'Fuscia', 4, 'Maroon', 'Chickie');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle, etage_plan_etage, batiment_plan_etage, laboratoire)
    VALUES ('http://csmonitor.com/diam/id/ornare/imperdiet/sapien.jsp', 'Berkley', '#3985fe', '#127af6', 5, 'Pink', 1, 'Goldenrod', 'Anitra');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle, etage_plan_etage, batiment_plan_etage, laboratoire)
    VALUES ('https://pinterest.com/magnis/dis/parturient/montes/nascetur.xml', 'Marris', '#a62c10', '#0b26a5', 5, 'Yellow', 2, 'Red', 'Chick');

INSERT INTO Imgs (url, TYPE, serveur, nom_plan_salle, etage_plan_salle, batiment_plan_salle, etage_plan_etage, batiment_plan_etage, laboratoire)
    VALUES ('http://usda.gov/ipsum/primis/in.js', 'Etheline', '#d2da84', '#3ff88c', 6, 'Red', 1, 'Crimson', 'Quintin');

---------------------------------Employes---------------------------------
----------------Laboratoire
----ML

INSERT INTO Employes (num_badge, nom, prenom, email, statut_employe, organisation, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('#2280ef', 'Saltrese', 'Shannan', 'ssaltrese0@pro.jp', 'CDI', 'ML', '#13bb79', 1, 'Green');

----MGL
INSERT INTO Employes (num_badge, nom, prenom, email, statut_employe, organisation, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('#339d4e', 'Beeching', 'Emlyn', 'ebeeching1@is.gd', 'CDI', 'MGL', '#86b429', 2, 'Green');

-----------------Departement
----SD

INSERT INTO Employes (num_badge, nom, prenom, email, statut_employe, organisation, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('#5a96a6', 'Brock', 'Issie', 'ibrock2@redcross.org', 'CDI', 'SD', '#62d26c', 1, 'Pink');

INSERT INTO Employes (num_badge, nom, prenom, email, statut_employe, organisation, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('#66d998', 'Moine', 'Inez', 'imoine3@psu.edu', 'CDI', 'SD', '#62d26c', 1, 'Pink');

INSERT INTO Employes (num_badge, nom, prenom, email, statut_employe, organisation, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('#b28783', 'Stannett', 'Hazel', 'hstannett9@domains.com', 'Stagiaire', 'SD', '#62d26c', 1, 'Pink');

----PMD
INSERT INTO Employes (num_badge, nom, prenom, email, statut_employe, organisation, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('#c3f470', 'Kingman', 'Thatcher', 'tkingman4@msu.edu', 'CDD', 'PMD', '#c951ef', 2, 'Pink');

----HRD
INSERT INTO Employes (num_badge, nom, prenom, email, statut_employe, organisation, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('#c114ef', 'Gehricke', 'Adan', 'agehricke5@collective.com', 'CDD', 'HRD', '#57f26c', 2, 'Pink');

----SE
INSERT INTO Employes (num_badge, nom, prenom, email, statut_employe, organisation, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('#35cdca', 'Odeson', 'Cherice', 'codeson6@japanpost.jp', 'CDD', 'SE', '#1244cf', 3, 'Pink');

----MD
INSERT INTO Employes (num_badge, nom, prenom, email, statut_employe, organisation, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('#5e520d', 'Bayston', 'Winslow', 'wbayston7@arizona.edu', 'CDD', 'MD', '#872472', 5, 'Green');

----MGD
INSERT INTO Employes (num_badge, nom, prenom, email, statut_employe, organisation, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('#205f9e', 'Johansson', 'Ashil', 'ajohansson8@spotify.com', 'Stagiaire', 'MGD', '#3582a8', 4, 'Green');

---------------------------------Statut_employe---------------------------------
INSERT INTO Statut_employe (TYPE)
    VALUES ('CDI');

INSERT INTO Statut_employe (TYPE)
    VALUES ('CDD');

INSERT INTO Statut_employe (TYPE)
    VALUES ('Stagiaire');

---------------------------------Organisations---------------------------------
INSERT INTO Organisations (sigle, nom, TYPE, thematique_etude)
    VALUES ('ML', 'Microbiologie', 'L', 'Microbiologie');

INSERT INTO Organisations (sigle, nom, TYPE, thematique_etude)
    VALUES ('MGL', 'Manipulation génétique', 'L', 'Manipulation génétique');

--- data érronné
INSERT INTO Organisations (sigle, nom, TYPE, domaine_expertise)
    VALUES ('MGL', 'Manipulation génétique', 'L', 'Manipulation génétique');

------------------------
INSERT INTO Organisations (sigle, nom, TYPE, domaine_expertise)
    VALUES ('PMD', 'Product Management', 'D', 'Product Management');

INSERT INTO Organisations (sigle, nom, TYPE, domaine_expertise)
    VALUES ('MD', 'Microbiologie', 'D', 'Microbiologie');

INSERT INTO Organisations (sigle, nom, TYPE, domaine_expertise)
    VALUES ('MGD', 'Manipulation génétique', 'D', 'Manipulation génétique');

INSERT INTO Organisations (sigle, nom, TYPE, domaine_expertise)
    VALUES ('HRD', 'Human Resources', 'D', 'Human Resources');

INSERT INTO Organisations (sigle, nom, TYPE, domaine_expertise)
    VALUES ('SD', 'Support', 'D', 'Support');

INSERT INTO Organisations (sigle, nom, TYPE, domaine_expertise)
    VALUES ('SE', 'Engineering', 'D', 'Engineering');

---------------------------------Thematiques_etude---------------------------------
INSERT INTO Thematiques_etude (TYPE)
    VALUES ('Microbiologie');

INSERT INTO Thematiques_etude (TYPE)
    VALUES ('Manipulation génétique');

---------------------------------Domaines_expertise---------------------------------
INSERT INTO Domaines_expertise (TYPE)
    VALUES ('Engineering');

INSERT INTO Domaines_expertise (TYPE)
    VALUES ('Product Management');

INSERT INTO Domaines_expertise (TYPE)
    VALUES ('Microbiologie');

INSERT INTO Domaines_expertise (TYPE)
    VALUES ('Manipulation génétique');

INSERT INTO Domaines_expertise (TYPE)
    VALUES ('Human Resources');

INSERT INTO Domaines_expertise (TYPE)
    VALUES ('Support');

---------------------------------Besoins---------------------------------
INSERT INTO Besoins (id, puissance_elec, besoin_triphase, besoin_reseau, besoin_gaz)
    VALUES (1, 23, TRUE, TRUE, 'Butane');

INSERT INTO Besoins (id, puissance_elec, besoin_triphase, besoin_reseau, besoin_gaz)
    VALUES (2, 29, TRUE, TRUE, 'Méthane');

INSERT INTO Besoins (id, puissance_elec, besoin_triphase, besoin_reseau, besoin_gaz)
    VALUES (3, 28, TRUE, TRUE, 'O2');

INSERT INTO Besoins (id, puissance_elec, besoin_triphase, besoin_reseau, besoin_gaz)
    VALUES (4, 21, TRUE, TRUE, 'Azote');

INSERT INTO Besoins (id, puissance_elec, besoin_triphase, besoin_reseau, besoin_gaz)
    VALUES (5, 21, TRUE, TRUE, 'None');

INSERT INTO Besoins (id, puissance_elec, besoin_triphase, besoin_reseau, besoin_gaz)
    VALUES (6, 21, FALSE, FALSE, 'None');

---------------------------------Machines---------------------------------
INSERT INTO Machines (num_contrat, modele, description, entreprise_maintenance, taille_machine, nom_salle, etage_salle, batiment_salle)
    VALUES ('#46989e', 1, 0.0173131555, 'Zoonder', 14, '#7320a7', 6, 'Pink');

INSERT INTO Machines (num_contrat, modele, description, entreprise_maintenance, taille_machine, nom_salle, etage_salle, batiment_salle)
    VALUES ('#c55b99', 2, 0.9137931996, 'Mybuzz', 16, '#7320a7', 6, 'Pink');

INSERT INTO Machines (num_contrat, modele, description, entreprise_maintenance, taille_machine, nom_salle, etage_salle, batiment_salle)
    VALUES ('#962ab0', 3, 0.4508982729, 'Mydeo', 15, '#7320a7', 6, 'Pink');

INSERT INTO Machines (num_contrat, modele, description, entreprise_maintenance, taille_machine, nom_salle, etage_salle, batiment_salle)
    VALUES ('#598cbe', 4, 0.1402635205, 'Photobug', 16, '#58366f', 6, 'Pink');

INSERT INTO Machines (num_contrat, modele, description, entreprise_maintenance, taille_machine, nom_salle, etage_salle, batiment_salle)
    VALUES ('#0e0d98', 5, 0.3356958852, 'Thoughtsphere', 18, '#58366f', 6, 'Pink');

---------------------------------Postes_tele---------------------------------
INSERT INTO Postes_tele (num_interne, num_direct, TYPE, modele_tele, marque, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('59779-196', '50600-001', 'V', 'jcb', 'cough', '#9b8400', 5, 'Pink');

INSERT INTO Postes_tele (num_interne, num_direct, TYPE, modele_tele, marque, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('54235-208', '50201-2400', 'V', 'instapayment', 'FRAICHEUR', '#9b8400', 5, 'Pink');

INSERT INTO Postes_tele (num_interne, num_direct, TYPE, modele_tele, marque, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('57520-0313', '63868-763', 'T', 'visa-electron', 'Tendon Rescue', '#9b8400', 5, 'Pink');

INSERT INTO Postes_tele (num_interne, num_direct, TYPE, modele_tele, marque, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('54868-6279', '0615-7641', 'T', 'mastercard', 'Levofloxacin', '#8a0e59', 5, 'Pink');

INSERT INTO Postes_tele (num_interne, num_direct, TYPE, modele_tele, marque, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('76485-1003', '60575-515', 'L', 'jcb', 'Moisture Renew', '#8a0e59', 5, 'Pink');

INSERT INTO Postes_tele (num_interne, num_direct, TYPE, modele_tele, marque, nom_bureau, etage_bureau, batiment_bureau)
    VALUES ('55154-3344', '24385-976', 'L', 'jcb', 'Ziprasidone', '#8a0e59', 5, 'Pink');

---------------------------------PC_fixes---------------------------------
INSERT INTO PC_fixes (nom, os, responsable, adresse_MAC, adresse_IP, nom_salle, etage_salle, batiment_salle)
    VALUES ('#6c04be', 'Linux', '#5a96a6', '14-B7-AF-C0-ED-93', '113.195.172.75', '#369bbd', 3, 'Green');

INSERT INTO PC_fixes (nom, os, responsable, adresse_MAC, adresse_IP, nom_salle, etage_salle, batiment_salle)
    VALUES ('#2a6d49', 'Linux', '#66d998', 'AC-EA-A6-B8-5C-CF', '39.240.175.113', '#369bbd', 3, 'Green');

INSERT INTO PC_fixes (nom, os, responsable, adresse_MAC, adresse_IP, nom_salle, etage_salle, batiment_salle)
    VALUES ('#4476b5', 'Linux', '#b28783', 'C0-56-94-E3-0C-1B', '72.245.163.179', '#369bbd', 3, 'Green');

---------------------------------PC_portables---------------------------------
INSERT INTO PC_portables (nom, os, responsable, adresse_MAC, adresse_IP, nom_salle, etage_salle, batiment_salle)
    VALUES ('#2cdd44', 'Windows', '#5a96a6', '7C-7D-0A-D7-E2-58', '229.172.131.43', '#5b3eab', 3, 'Green');

INSERT INTO PC_portables (nom, os, responsable, adresse_MAC, adresse_IP, nom_salle, etage_salle, batiment_salle)
    VALUES ('#1c9d7e', 'Windows', '#66d998', 'F3-E1-CE-8A-55-D6', '83.14.9.13', '#5b3eab', 3, 'Green');

INSERT INTO PC_portables (nom, os, responsable, adresse_MAC, adresse_IP, ordi_fonction)
    VALUES ('#163d10', 'Windows', '#b28783', '20-69-B3-74-71-65', '21.226.199.145', '#c3f470');

---------------------------------PC_serveurs---------------------------------
INSERT INTO PC_serveurs (nom, responsable, nom_salle, etage_salle, batiment_salle)
    VALUES ('#4fa757', '#5a96a6', '#5b3eab', 3, 'Green');

INSERT INTO PC_serveurs (nom, responsable, nom_salle, etage_salle, batiment_salle)
    VALUES ('#a4e308', '#66d998', '#369bbd', 3, 'Green');

INSERT INTO PC_serveurs (nom, responsable, nom_salle, etage_salle, batiment_salle)
    VALUES ('#218a47', '#b28783', '#369bbd', 3, 'Green');

---------------------------------Adresse_MAC_serveur---------------------------------
INSERT INTO Adresse_MAC_serveur (adresse_MAC, serveur)
    VALUES ('CA-D0-1A-75-77-DB', '#4fa757');

INSERT INTO Adresse_MAC_serveur (adresse_MAC, serveur)
    VALUES ('8C-B3-28-12-CD-1C', '#4fa757');

INSERT INTO Adresse_MAC_serveur (adresse_MAC, serveur)
    VALUES ('9A-D8-F0-75-32-3E', '#a4e308');

INSERT INTO Adresse_MAC_serveur (adresse_MAC, serveur)
    VALUES ('41-2F-E7-61-12-7A', '#a4e308');

INSERT INTO Adresse_MAC_serveur (adresse_MAC, serveur)
    VALUES ('91-B1-8B-AA-F6-58', '#218a47');

INSERT INTO Adresse_MAC_serveur (adresse_MAC, serveur)
    VALUES ('67-D7-03-ED-33-10', '#218a47');

---------------------------------Adresse_IP_serveur---------------------------------
INSERT INTO Adresse_IP_serveur (adresse_IP, adresse_MAC)
    VALUES ('97.164.67.22', 'CA-D0-1A-75-77-DB');

INSERT INTO Adresse_IP_serveur (adresse_IP, adresse_MAC)
    VALUES ('213.27.10.62', 'CA-D0-1A-75-77-DB');

INSERT INTO Adresse_IP_serveur (adresse_IP, adresse_MAC)
    VALUES ('225.136.52.164', '8C-B3-28-12-CD-1C');

INSERT INTO Adresse_IP_serveur (adresse_IP, adresse_MAC)
    VALUES ('183.49.117.67', '8C-B3-28-12-CD-1C');

INSERT INTO Adresse_IP_serveur (adresse_IP, adresse_MAC)
    VALUES ('61.86.103.26', '9A-D8-F0-75-32-3E');

INSERT INTO Adresse_IP_serveur (adresse_IP, adresse_MAC)
    VALUES ('89.99.120.97', '9A-D8-F0-75-32-3E');

INSERT INTO Adresse_IP_serveur (adresse_IP, adresse_MAC)
    VALUES ('175.189.16.18', '41-2F-E7-61-12-7A');

INSERT INTO Adresse_IP_serveur (adresse_IP, adresse_MAC)
    VALUES ('24.78.28.75', '41-2F-E7-61-12-7A');

INSERT INTO Adresse_IP_serveur (adresse_IP, adresse_MAC)
    VALUES ('103.233.20.171', '91-B1-8B-AA-F6-58');

INSERT INTO Adresse_IP_serveur (adresse_IP, adresse_MAC)
    VALUES ('224.102.170.247', '91-B1-8B-AA-F6-58');

INSERT INTO Adresse_IP_serveur (adresse_IP, adresse_MAC)
    VALUES ('75.195.66.169', '67-D7-03-ED-33-10');

INSERT INTO Adresse_IP_serveur (adresse_IP, adresse_MAC)
    VALUES ('108.16.113.231', '67-D7-03-ED-33-10');

---------------------------------Projets---------------------------------
INSERT INTO Projets (sigle, nom, description, start_date, end_date, chef)
    VALUES ('EUR', 'Agnes', 'Other anaphylactic react', '2020-04-09', '2021-11-05', '#2280ef');

INSERT INTO Projets (sigle, nom, description, start_date, end_date, chef)
    VALUES ('CNY', 'Pearl', 'Disproportion NOS-unspec', '2019-06-04', '2021-10-09', '#339d4e');

INSERT INTO Projets (sigle, nom, description, start_date, end_date, chef)
    VALUES ('HRK', 'Keith', 'Blister forearm', '2019-12-29', '2021-03-19', '#c3f470');

INSERT INTO Projets (sigle, nom, description, start_date, end_date, chef)
    VALUES ('ACD', 'structe', 'Blister forearm', '2019-12-29', '2021-03-19', '#c3f470');

INSERT INTO Projets (sigle, nom, description, start_date, end_date, chef)
    VALUES ('ABC', 'base de donnée', 'Blister forearm', '2019-12-29', '2021-03-19', '#c3f470');

---------------------------------Taches---------------------------------
INSERT INTO Taches (id, projet, start_date)
    VALUES (1, 'EUR', '2020-12-01');

INSERT INTO Taches (id, projet, start_date, end_date)
    VALUES (2, 'EUR', '2020-01-05', '2021-03-02');

INSERT INTO Taches (id, projet, start_date, end_date)
    VALUES (1, 'CNY', '2020-04-18', '2020-05-21');

INSERT INTO Taches (id, projet, start_date, end_date)
    VALUES (2, 'CNY', '2020-12-12', '2021-04-13');

INSERT INTO Taches (id, projet, start_date)
    VALUES (1, 'HRK', '2020-01-04');

---------------------------------Participe---------------------------------
INSERT INTO Participe (employe, id_tache, projet, ROLE)
    VALUES ('#35cdca', 1, 'EUR', 'Engineering');

INSERT INTO Participe (employe, id_tache, projet, ROLE)
    VALUES ('#35cdca', 1, 'CNY', 'Engineering');

INSERT INTO Participe (employe, id_tache, projet, ROLE)
    VALUES ('#5e520d', 1, 'EUR', 'Microbiologie');

INSERT INTO Participe (employe, id_tache, projet, ROLE)
    VALUES ('#339d4e', 2, 'EUR', 'Microbiologie');

INSERT INTO Participe (employe, id_tache, projet, ROLE)
    VALUES ('#c3f470', 2, 'CNY', 'Microbiologie');

INSERT INTO Participe (employe, id_tache, projet, ROLE)
    VALUES ('#c3f470', 1, 'EUR', 'Microbiologie');

INSERT INTO Participe (employe, id_tache, projet, ROLE)
    VALUES ('#c3f470', 2, 'EUR', 'Microbiologie');

INSERT INTO Participe (employe, id_tache, projet, ROLE)
    VALUES ('#c3f470', 1, 'HRK', 'Microbiologie');

SELECT
    *
FROM
    Employes;

