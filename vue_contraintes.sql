/* la vue ne doit comporter aucun element ! */
-- 1. CC1: Projection(Etages, batiment) = Projection(Batiments, nom)
-- Batiments 1-- "1..N" Etages

CREATE VIEW VBatimentSansEtage AS
SELECT
    nom
FROM
    Batiments
EXCEPT
SELECT
    batiment
FROM
    Etages;

SELECT
    *
FROM
    VBatimentSansEtage;

-- Intersection(Projection(Jointure(Restriction(Employes, organisation_secondaire != null), Organisations, E.organisation = O.sigle), type)  ,Projection(Jointure(Employes, Organisations, E.organisation_secondaire = O.sigle), type)) = Ø
CREATE VIEW VOrganisationDifferente AS
SELECT
    o.type
FROM
    Employes AS e
    JOIN Organisations AS o ON e.organisation = o.sigle
        AND e.organisation_secondaire IS NOT NULL
    INTERSECT
    SELECT
        o.type
    FROM
        Employes AS e
    JOIN Organisations AS o ON e.organisation_secondaire = o.sigle
        AND e.organisation_secondaire IS NOT NULL;

DROP VIEW VOrganisationDifferente;

SELECT
    *
FROM
    VOrganisationDifferente;

-- 2. CC2: Projection (Etages, num_etage) = Projection (Salles, etage)
-- Etages 1 -- "1..N" Salles

CREATE VIEW VEtageSansSalle AS
SELECT
    num_etage
FROM
    Etages
EXCEPT
SELECT
    etage
FROM
    Salles;

SELECT
    *
FROM
    VEtageSansSalle;

-- Intersection (Projection (PC_fixes), nom), Projection (PC_portables, nom), Projection (PC_serveurs, nom)) = Ø
CREATE VIEW VPC_fixeExclutPC_portable AS
SELECT
    nom
FROM
    PC_fixes
INTERSECT
SELECT
    nom
FROM
    PC_portables;

CREATE VIEW VPC_fixeExclutPC_serveur AS
SELECT
    nom
FROM
    PC_fixes
INTERSECT
SELECT
    nom
FROM
    PC_serveurs;

CREATE VIEW VPC_portableExclutPC_serveur AS
SELECT
    nom
FROM
    PC_portables
INTERSECT
SELECT
    nom
FROM
    PC_serveurs;

-- vMoyen_Info = Union (Projection (PC_fixes), nom), Projection (PC_portables, nom), Projection (PC_serveurs, nom)) = Ø
CREATE VIEW VMoyen_Info AS
SELECT
    nom,
    responsable
FROM
    PC_fixes
UNION
SELECT
    nom,
    responsable
FROM
    PC_portables
UNION
SELECT
    nom,
    responsable
FROM
    PC_serveurs;

DROP VIEW VMoyen_Info;

SELECT
    *
FROM
    vMoyen_Info;

-- projection(PC_serveurs, nom) = Projection(Adresse_MAC_serveur, serveur)
-- PC_serveurs "1" --- "1..N" Adresse_MAC_serveur : > a

CREATE VIEW VPC_serveurSansMAC AS
SELECT
    nom
FROM
    PC_serveurs
EXCEPT
SELECT
    serveur
FROM
    Adresse_MAC_serveur;

-- Projection(Adresse_MAC_serveur, adresse_MAC) = projection(Adresse_Ip_serveur, adresse_MAC)
CREATE VIEW VMACSansIP AS
SELECT
    adresse_MAC
FROM
    Adresse_MAC_serveur
EXCEPT
SELECT
    adresse_MAC
FROM
    Adresse_Ip_serveur;

SELECT
    *
FROM
    VMACSansIP;

-- Projection(Projets, sigle) = Projection(Taches, projet)
-- Projets 1-- "1..N" Taches

CREATE VIEW VProjetSansTache AS
SELECT
    sigle
FROM
    Projets
EXCEPT
SELECT
    projet
FROM
    Taches;

-- Restriction(Jointure(Projets, Taches, p.sigle = t.projet), t.start_date < p.start_date ) = Ø
CREATE VIEW VStartDateContrainte AS
SELECT
    sigle
FROM
    Projets AS p
    JOIN Taches AS t ON p.sigle = t.projet
WHERE
    t.start_date < p.start_date;

SELECT
    *
FROM
    VStartDateContrainte;

SELECT
    *
FROM
    Projets;

SELECT
    *
FROM
    Taches;

-- Restriction(Jointure(Projets, Taches, p.sigle = t.projet), t.end_date > p.end_date ) = Ø
CREATE VIEW VEndDateContrainte AS
SELECT
    sigle
FROM
    Projets AS p
    JOIN Taches AS t ON p.sigle = t.projet
WHERE
    t.end_date > p.end_date;

SELECT
    *
FROM
    VEndDateContrainte;

-------------------------------------------Des classes spécifique-------------------------------------------
-- vLaboratoires = Restriction(Organisations, type = "L")

CREATE VIEW vLaboratoires AS
SELECT
    *
FROM
    Organisations
WHERE
    TYPE = 'L';

-- vDepartements = Restriction(Organisations, type = "D")
CREATE VIEW vDepartements AS
SELECT
    *
FROM
    Organisations
WHERE
    TYPE = 'D';

-- vDirecteur = Projection (Jointure(Employes, Organisations, E.num_badge = O.directeur), E)
CREATE VIEW vDirecteur AS
SELECT
    e.nom AS nom_employe,
    e.prenom AS prenom_employe,
    e.num_badge AS num_badge_employe,
    o.nom AS nom_organisation,
    o.sigle
FROM
    Employes AS e
    JOIN Organisations AS o ON e.num_badge = o.directeur;

DROP VIEW vDirecteur;

SELECT
    *
FROM
    vDirecteur;

-- vMembre = Projection(Jointure(Employes, Organisations, E.organisation = O.sigle), E)
CREATE VIEW vMembre AS
SELECT
    e.nom AS nom_employe,
    e.prenom AS prenom_employe,
    e.num_badge AS num_badge_employe,
    o.nom AS nom_organisation,
    o.sigle
FROM
    Employes AS e
    JOIN Organisations AS o ON e.organisation = o.sigle;

DROP VIEW vMembre;

SELECT
    *
FROM
    vMembre;

